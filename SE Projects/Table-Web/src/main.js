import Vue from 'vue'
import App from './App'
import router from './router'
import AsyncComputed from 'vue-async-computed'

Vue.config.productionTip = false

Vue.use(AsyncComputed)
new Vue({
  el: '#app',
  router,
  render: h => h(App)
})
