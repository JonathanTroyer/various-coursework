export class Byte {
	constructor(val) {
		this.value = val
	}

	get bin() {
		return ('0000000' + this.value.toString(2)).slice(-8)
	}

	set bin(val) {
		this.value = parseInt(val, 2)
	}

	get dec() {
		return this.value.toString(10)
	}

	set dec(val) {
		this.value = parseInt(val, 10)
	}

	get hex() {
		return ('0' + this.value.toString(16)).slice(-2)
	}

	set hex(val) {
		this.value = parseInt(val, 16)
	}

	get value() {
		return this._value
	}

	set value(val) {
		if (val <= 255 && val >= 0)
			this._value = val
	}
}

export class Word {
	constructor(val) {
		this.value = val
	}

	get hex() {
		return ('0000000' + this.value.toString(16)).slice(-8)
	}

	set hex(val) {
		this.value = parseInt(val, 16)
	}

	get dec() {
		return this.value.toString(10)
	}

	set dec(val) {
		this.value = parseInt(val, 10)
	}
}

export class MemmoryManager {
	constructor(size) {
		this.memory = new Array()
		for (let i = 0; i < size; i++) {
			this.memory.push(new Byte(0))
		}
	}

	readWord(index) {
		let temp = 0;
		if (this.memory && index < this.memory.length - 3) {
			temp = ('0' + this.memory[index].value.toString(16)).slice(-2)
			temp += ('0' + this.memory[index + 1].value.toString(16)).slice(-2)
			temp += ('0' + this.memory[index + 2].value.toString(16)).slice(-2)
			temp += ('0' + this.memory[index + 3].value.toString(16)).slice(-2)
		}
		return temp
	}

	writeWord(index, word) {
		this.memory[index].value = (word >>> 24) & 0xFF
		this.memory[index + 1].value = (word >>> 16) & 0xFF
		this.memory[index + 2].value = (word >>> 8) & 0xFF
		this.memory[index + 3].value = word & 0xFF
	}
}
