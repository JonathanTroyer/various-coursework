import {Matrix4, Vector3} from "./matrix";

export class Camera {
  constructor() {
    this.perspective = true;
    this.near = 1;
    this.far = 100;
    this.fov = 60;
    this.ratio = 1;
    this.view = new Matrix4();
    this.projection = new Matrix4();

    this.eye = new Vector3(0, 5, 10, 1);
    this.lookAt = new Vector3(0, 0, 0, 1);
    this.u = new Vector3(1, 0, 0, 0);
    this.v = new Vector3(0, 1, 0, 0);
    this.n = new Vector3(0, 0, 1, 0);

    this.rTheta = 0;
    this.pTheta = 0;
    this.yTheta = 0;

    this.recalcView();
    this.setLookAt();
  }

  get matrix() {
    let tmp = new Matrix4(this.projection);
    return tmp.multiply(this.view);
  }

  recalcView() {
    if (this.perspective) {
      this.setPerspective();
    } else {
      this.setOrtho();
    }

    this.setLookAt();
  }

  setPerspective() {
    if (this.ratio === 0) {
      throw 'Null frustum';
    }
    if (this.near <= 0 || this.far <= 0) {
      throw 'Invalid viewing plane';
    }
    let fov = Math.PI * this.fov / 180 / 2;
    let s = Math.sin(fov);
    if (s === 0) {
      throw 'Null frustum';
    }

    let rd = 1 / (this.far - this.near);
    let ct = Math.cos(fov) / s;

    this.projection.elements[0] = ct / this.ratio;
    this.projection.elements[1] = 0;
    this.projection.elements[2] = 0;
    this.projection.elements[3] = 0;

    this.projection.elements[4] = 0;
    this.projection.elements[5] = ct;
    this.projection.elements[6] = 0;
    this.projection.elements[7] = 0;

    this.projection.elements[8] = 0;
    this.projection.elements[9] = 0;
    this.projection.elements[10] = -(this.far + this.near) * rd;
    this.projection.elements[11] = -1;

    this.projection.elements[12] = 0;
    this.projection.elements[13] = 0;
    this.projection.elements[14] = -2 * this.near * this.far * rd;
    this.projection.elements[15] = 0;
  }

  setOrtho() {
    let left = -1, right = 1, top = 1, bottom = -1;
    if (left === right || bottom === top || this.near === this.far) {
      throw 'Null frustum';
    }

    let rw = 1 / (right - left);
    let rh = 1 / (top - bottom);
    let rd = 1 / (this.far - this.near);

    this.projection.elements[0] = 2 * rw;
    this.projection.elements[1] = 0;
    this.projection.elements[2] = 0;
    this.projection.elements[3] = 0;

    this.projection.elements[4] = 0;
    this.projection.elements[5] = 2 * rh;
    this.projection.elements[6] = 0;
    this.projection.elements[7] = 0;

    this.projection.elements[8] = 0;
    this.projection.elements[9] = 0;
    this.projection.elements[10] = -2 * rd;
    this.projection.elements[11] = 0;

    this.projection.elements[12] = -(right + left) * rw;
    this.projection.elements[13] = -(top + bottom) * rh;
    this.projection.elements[14] = -(this.far + this.near) * rd;
    this.projection.elements[15] = 1;
  }

  setLookAt() {
    let fx, fy, fz, rlf, sx, sy, sz, rls, ux, uy, uz;
    fx = this.lookAt.x - this.eye.x;
    fy = this.lookAt.y - this.eye.y;
    fz = this.lookAt.z - this.eye.z;

    // Normalize f.
    rlf = 1 / Math.sqrt(fx * fx + fy * fy + fz * fz);
    fx *= rlf;
    fy *= rlf;
    fz *= rlf;

    // Calculate cross product of f and up.
    sx = fy * this.v.z - fz * this.v.y;
    sy = fz * this.v.x - fx * this.v.z;
    sz = fx * this.v.y - fy * this.v.x;

    // Normalize s.
    rls = 1 / Math.sqrt(sx * sx + sy * sy + sz * sz);
    sx *= rls;
    sy *= rls;
    sz *= rls;

    // Calculate cross product of s and f.
    ux = sy * fz - sz * fy;
    uy = sz * fx - sx * fz;
    uz = sx * fy - sy * fx;

    // Set to this.
    this.view.elements[0] = sx;
    this.view.elements[1] = ux;
    this.view.elements[2] = -fx;
    this.view.elements[3] = 0;

    this.view.elements[4] = sy;
    this.view.elements[5] = uy;
    this.view.elements[6] = -fy;
    this.view.elements[7] = 0;

    this.view.elements[8] = sz;
    this.view.elements[9] = uz;
    this.view.elements[10] = -fz;
    this.view.elements[11] = 0;

    this.view.elements[12] = 0;
    this.view.elements[13] = 0;
    this.view.elements[14] = 0;
    this.view.elements[15] = 1;

    return this.view.translate(-this.eye.x, -this.eye.y, -this.eye.z);
  }

  translateU(amt) {
    this.eye.x += this.u.x * amt;
    this.eye.y += this.u.y * amt;
    this.eye.z += this.u.z * amt;

    this.lookAt.x += this.u.x * amt;
    this.lookAt.y += this.u.y * amt;
    this.lookAt.z += this.u.z * amt;

    this.setLookAt();
  }

  translateV(amt) {
    this.eye.x += this.v.x * amt;
    this.eye.y += this.v.y * amt;
    this.eye.z += this.v.z * amt;

    this.lookAt.x += this.v.x * amt;
    this.lookAt.y += this.v.y * amt;
    this.lookAt.z += this.v.z * amt;

    this.setLookAt();
  }

  translateN(amt) {
    this.eye.x += this.n.x * amt;
    this.eye.y += this.n.y * amt;
    this.eye.z += this.n.z * amt;

    this.lookAt.x += this.n.x * amt;
    this.lookAt.y += this.n.y * amt;
    this.lookAt.z += this.n.z * amt;

    this.setLookAt();
  }

  roll(amt) {
    this.rTheta += amt;
    this.rTheta %= 360;
    let mat = (new Matrix4())
      .setTranslate(this.eye.x, this.eye.y, this.eye.z)
      .rotate(this.rTheta, this.n.x, this.n.y, this.n.z)
      .translate(-this.eye.x, -this.eye.y, -this.eye.z);

    this.lookAt.multiply(mat);
    this.u.multiply(mat);
    this.v.multiply(mat);

    this.setLookAt();
  }

  pitch(amt) {
    this.pTheta += amt;
    this.pTheta %= 360;
    let mat = (new Matrix4())
      .setTranslate(this.eye.x, this.eye.y, this.eye.z)
      .rotate(this.pTheta, this.u.x, this.u.y, this.u.z)
      .translate(-this.eye.x, -this.eye.y, -this.eye.z);


    this.lookAt.multiply(mat);
    this.v.multiply(mat);
    this.n.multiply(mat);

    this.setLookAt();
  }

  yaw(amt) {
    this.yTheta += amt;
    this.yTheta %= 360;
    let mat = (new Matrix4())
      .setTranslate(this.eye.x, this.eye.y, this.eye.z)
      .rotate(this.yTheta, this.v.x, this.v.y, this.v.z)
      .translate(-this.eye.x, -this.eye.y, -this.eye.z);

    this.lookAt.multiply(mat);
    this.u.multiply(mat);
    this.n.multiply(mat);

    this.setLookAt();
  }
}
