export class Matrix4 {

  constructor(other) {
    if (other != null)
      this.elements = other.elements.map(e => e);
    else
      this.elements = new Float32Array(16);

    return this;
  }

  setIdentity() {
    this.elements = new Float32Array([
      1, 0, 0, 0,
      0, 1, 0, 0,
      0, 0, 1, 0,
      0, 0, 0, 1
    ]);

    return this;
  }

  multiply(other) {
    if (typeof other === 'number') {
      this.elements = this.elements.map(e => e * other);
    } else {
      let ai0, ai1, ai2, ai3;
      let b = other.elements.slice();
      for (let i = 0; i < 4; i++) {
        ai0 = this.elements[i];
        ai1 = this.elements[i + 4];
        ai2 = this.elements[i + 8];
        ai3 = this.elements[i + 12];
        this.elements[i] = ai0 * b[0] + ai1 * b[1] + ai2 * b[2] + ai3 * b[3];
        this.elements[i + 4] = ai0 * b[4] + ai1 * b[5] + ai2 * b[6] + ai3 * b[7];
        this.elements[i + 8] = ai0 * b[8] + ai1 * b[9] + ai2 * b[10] + ai3 * b[11];
        this.elements[i + 12] = ai0 * b[12] + ai1 * b[13] + ai2 * b[14] + ai3 * b[15];
      }
    }

    return this;
  }

  transpose() {
    let t;
    t = this.elements[1];
    this.elements[1] = this.elements[4];
    this.elements[4] = t;
    t = this.elements[2];
    this.elements[2] = this.elements[8];
    this.elements[8] = t;
    t = this.elements[3];
    this.elements[3] = this.elements[12];
    this.elements[12] = t;
    t = this.elements[6];
    this.elements[6] = this.elements[9];
    this.elements[9] = t;
    t = this.elements[7];
    this.elements[7] = this.elements[13];
    this.elements[13] = t;
    t = this.elements[11];
    this.elements[11] = this.elements[14];
    this.elements[14] = t;

    return this;
  }

  setInverseOf(other) {
    let i, inv, det;
    inv = new Float32Array(16);

    inv[0] = other.elements[5] * other.elements[10] * other.elements[15] - other.elements[5] * other.elements[11] * other.elements[14] - other.elements[9] * other.elements[6] * other.elements[15]
      + other.elements[9] * other.elements[7] * other.elements[14] + other.elements[13] * other.elements[6] * other.elements[11] - other.elements[13] * other.elements[7] * other.elements[10];
    inv[4] = -other.elements[4] * other.elements[10] * other.elements[15] + other.elements[4] * other.elements[11] * other.elements[14] + other.elements[8] * other.elements[6] * other.elements[15]
      - other.elements[8] * other.elements[7] * other.elements[14] - other.elements[12] * other.elements[6] * other.elements[11] + other.elements[12] * other.elements[7] * other.elements[10];
    inv[8] = other.elements[4] * other.elements[9] * other.elements[15] - other.elements[4] * other.elements[11] * other.elements[13] - other.elements[8] * other.elements[5] * other.elements[15]
      + other.elements[8] * other.elements[7] * other.elements[13] + other.elements[12] * other.elements[5] * other.elements[11] - other.elements[12] * other.elements[7] * other.elements[9];
    inv[12] = -other.elements[4] * other.elements[9] * other.elements[14] + other.elements[4] * other.elements[10] * other.elements[13] + other.elements[8] * other.elements[5] * other.elements[14]
      - other.elements[8] * other.elements[6] * other.elements[13] - other.elements[12] * other.elements[5] * other.elements[10] + other.elements[12] * other.elements[6] * other.elements[9];

    inv[1] = -other.elements[1] * other.elements[10] * other.elements[15] + other.elements[1] * other.elements[11] * other.elements[14] + other.elements[9] * other.elements[2] * other.elements[15]
      - other.elements[9] * other.elements[3] * other.elements[14] - other.elements[13] * other.elements[2] * other.elements[11] + other.elements[13] * other.elements[3] * other.elements[10];
    inv[5] = other.elements[0] * other.elements[10] * other.elements[15] - other.elements[0] * other.elements[11] * other.elements[14] - other.elements[8] * other.elements[2] * other.elements[15]
      + other.elements[8] * other.elements[3] * other.elements[14] + other.elements[12] * other.elements[2] * other.elements[11] - other.elements[12] * other.elements[3] * other.elements[10];
    inv[9] = -other.elements[0] * other.elements[9] * other.elements[15] + other.elements[0] * other.elements[11] * other.elements[13] + other.elements[8] * other.elements[1] * other.elements[15]
      - other.elements[8] * other.elements[3] * other.elements[13] - other.elements[12] * other.elements[1] * other.elements[11] + other.elements[12] * other.elements[3] * other.elements[9];
    inv[13] = other.elements[0] * other.elements[9] * other.elements[14] - other.elements[0] * other.elements[10] * other.elements[13] - other.elements[8] * other.elements[1] * other.elements[14]
      + other.elements[8] * other.elements[2] * other.elements[13] + other.elements[12] * other.elements[1] * other.elements[10] - other.elements[12] * other.elements[2] * other.elements[9];

    inv[2] = other.elements[1] * other.elements[6] * other.elements[15] - other.elements[1] * other.elements[7] * other.elements[14] - other.elements[5] * other.elements[2] * other.elements[15]
      + other.elements[5] * other.elements[3] * other.elements[14] + other.elements[13] * other.elements[2] * other.elements[7] - other.elements[13] * other.elements[3] * other.elements[6];
    inv[6] = -other.elements[0] * other.elements[6] * other.elements[15] + other.elements[0] * other.elements[7] * other.elements[14] + other.elements[4] * other.elements[2] * other.elements[15]
      - other.elements[4] * other.elements[3] * other.elements[14] - other.elements[12] * other.elements[2] * other.elements[7] + other.elements[12] * other.elements[3] * other.elements[6];
    inv[10] = other.elements[0] * other.elements[5] * other.elements[15] - other.elements[0] * other.elements[7] * other.elements[13] - other.elements[4] * other.elements[1] * other.elements[15]
      + other.elements[4] * other.elements[3] * other.elements[13] + other.elements[12] * other.elements[1] * other.elements[7] - other.elements[12] * other.elements[3] * other.elements[5];
    inv[14] = -other.elements[0] * other.elements[5] * other.elements[14] + other.elements[0] * other.elements[6] * other.elements[13] + other.elements[4] * other.elements[1] * other.elements[14]
      - other.elements[4] * other.elements[2] * other.elements[13] - other.elements[12] * other.elements[1] * other.elements[6] + other.elements[12] * other.elements[2] * other.elements[5];

    inv[3] = -other.elements[1] * other.elements[6] * other.elements[11] + other.elements[1] * other.elements[7] * other.elements[10] + other.elements[5] * other.elements[2] * other.elements[11]
      - other.elements[5] * other.elements[3] * other.elements[10] - other.elements[9] * other.elements[2] * other.elements[7] + other.elements[9] * other.elements[3] * other.elements[6];
    inv[7] = other.elements[0] * other.elements[6] * other.elements[11] - other.elements[0] * other.elements[7] * other.elements[10] - other.elements[4] * other.elements[2] * other.elements[11]
      + other.elements[4] * other.elements[3] * other.elements[10] + other.elements[8] * other.elements[2] * other.elements[7] - other.elements[8] * other.elements[3] * other.elements[6];
    inv[11] = -other.elements[0] * other.elements[5] * other.elements[11] + other.elements[0] * other.elements[7] * other.elements[9] + other.elements[4] * other.elements[1] * other.elements[11]
      - other.elements[4] * other.elements[3] * other.elements[9] - other.elements[8] * other.elements[1] * other.elements[7] + other.elements[8] * other.elements[3] * other.elements[5];
    inv[15] = other.elements[0] * other.elements[5] * other.elements[10] - other.elements[0] * other.elements[6] * other.elements[9] - other.elements[4] * other.elements[1] * other.elements[10]
      + other.elements[4] * other.elements[2] * other.elements[9] + other.elements[8] * other.elements[1] * other.elements[6] - other.elements[8] * other.elements[2] * other.elements[5];

    det = other.elements[0] * inv[0] + other.elements[1] * inv[4] + other.elements[2] * inv[8] + other.elements[3] * inv[12];
    if (det === 0) {
      return this;
    }

    det = 1 / det;
    for (i = 0; i < 16; i++) {
      this.elements[i] = inv[i] * det;
    }

    return this;
  }

  invert() {
    return this.setInverseOf(this);
  }

  setScale(x, y, z) {
    this.elements[0] = x;
    this.elements[4] = 0;
    this.elements[8] = 0;
    this.elements[12] = 0;
    this.elements[1] = 0;
    this.elements[5] = y;
    this.elements[9] = 0;
    this.elements[13] = 0;
    this.elements[2] = 0;
    this.elements[6] = 0;
    this.elements[10] = z;
    this.elements[14] = 0;
    this.elements[3] = 0;
    this.elements[7] = 0;
    this.elements[11] = 0;
    this.elements[15] = 1;
    return this;
  }

  scale(x, y, z) {
    this.elements[0] *= x;
    this.elements[4] *= y;
    this.elements[8] *= z;
    this.elements[1] *= x;
    this.elements[5] *= y;
    this.elements[9] *= z;
    this.elements[2] *= x;
    this.elements[6] *= y;
    this.elements[10] *= z;
    this.elements[3] *= x;
    this.elements[7] *= y;
    this.elements[11] *= z;
    return this;
  }

  setTranslate(x, y, z) {
    this.elements[0] = 1;
    this.elements[4] = 0;
    this.elements[8] = 0;
    this.elements[12] = x;
    this.elements[1] = 0;
    this.elements[5] = 1;
    this.elements[9] = 0;
    this.elements[13] = y;
    this.elements[2] = 0;
    this.elements[6] = 0;
    this.elements[10] = 1;
    this.elements[14] = z;
    this.elements[3] = 0;
    this.elements[7] = 0;
    this.elements[11] = 0;
    this.elements[15] = 1;
    return this;
  }

  translate(x, y, z) {
    this.elements[12] += this.elements[0] * x + this.elements[4] * y + this.elements[8] * z;
    this.elements[13] += this.elements[1] * x + this.elements[5] * y + this.elements[9] * z;
    this.elements[14] += this.elements[2] * x + this.elements[6] * y + this.elements[10] * z;
    this.elements[15] += this.elements[3] * x + this.elements[7] * y + this.elements[11] * z;
    return this;
  }

  setRotate(angle, x, y, z) {
    angle = Math.PI * angle / 180;
    let s = Math.sin(angle);
    let c = Math.cos(angle);

    if (0 !== x && 0 === y && 0 === z) {
      // Rotation around X axis
      if (x < 0) {
        s = -s;
      }
      this.elements[0] = 1;
      this.elements[4] = 0;
      this.elements[8] = 0;
      this.elements[12] = 0;
      this.elements[1] = 0;
      this.elements[5] = c;
      this.elements[9] = -s;
      this.elements[13] = 0;
      this.elements[2] = 0;
      this.elements[6] = s;
      this.elements[10] = c;
      this.elements[14] = 0;
      this.elements[3] = 0;
      this.elements[7] = 0;
      this.elements[11] = 0;
      this.elements[15] = 1;
    } else if (0 === x && 0 !== y && 0 === z) {
      // Rotation around Y axis
      if (y < 0) {
        s = -s;
      }
      this.elements[0] = c;
      this.elements[4] = 0;
      this.elements[8] = s;
      this.elements[12] = 0;
      this.elements[1] = 0;
      this.elements[5] = 1;
      this.elements[9] = 0;
      this.elements[13] = 0;
      this.elements[2] = -s;
      this.elements[6] = 0;
      this.elements[10] = c;
      this.elements[14] = 0;
      this.elements[3] = 0;
      this.elements[7] = 0;
      this.elements[11] = 0;
      this.elements[15] = 1;
    } else if (0 === x && 0 === y && 0 !== z) {
      // Rotation around Z axis
      if (z < 0) {
        s = -s;
      }
      this.elements[0] = c;
      this.elements[4] = -s;
      this.elements[8] = 0;
      this.elements[12] = 0;
      this.elements[1] = s;
      this.elements[5] = c;
      this.elements[9] = 0;
      this.elements[13] = 0;
      this.elements[2] = 0;
      this.elements[6] = 0;
      this.elements[10] = 1;
      this.elements[14] = 0;
      this.elements[3] = 0;
      this.elements[7] = 0;
      this.elements[11] = 0;
      this.elements[15] = 1;
    } else {
      // Rotation around another axis
      let len = Math.sqrt(x * x + y * y + z * z);
      if (len !== 1) {
        let rlen = 1 / len;
        x *= rlen;
        y *= rlen;
        z *= rlen;
      }
      let nc = 1 - c;
      let xy = x * y;
      let yz = y * z;
      let zx = z * x;
      let xs = x * s;
      let ys = y * s;
      let zs = z * s;

      this.elements[0] = x * x * nc + c;
      this.elements[1] = xy * nc + zs;
      this.elements[2] = zx * nc - ys;
      this.elements[3] = 0;

      this.elements[4] = xy * nc - zs;
      this.elements[5] = y * y * nc + c;
      this.elements[6] = yz * nc + xs;
      this.elements[7] = 0;

      this.elements[8] = zx * nc + ys;
      this.elements[9] = yz * nc - xs;
      this.elements[10] = z * z * nc + c;
      this.elements[11] = 0;

      this.elements[12] = 0;
      this.elements[13] = 0;
      this.elements[14] = 0;
      this.elements[15] = 1;
    }

    return this;
  }

  rotate(angle, x, y, z) {
    return this.multiply(new Matrix4().setRotate(angle, x, y, z));
  }
}

export class Vector3 {
  constructor(x, y, z, w) {
    this.x = x;
    this.y = y;
    this.z = z;

    this.w = w;

    return this;
  }

  setX() {
    this.x = 1;
    this.y = 0;
    this.z = 0;

    return this;
  }

  setY() {
    this.x = 0;
    this.y = 1;
    this.z = 0;

    return this;
  }

  setZ() {
    this.x = 0;
    this.y = 0;
    this.z = 1;

    return this;
  }

  zero() {
    this.x = 0;
    this.y = 0;
    this.z = 0;

    return this;
  }

  magnitude() {
    return Math.sqrt(this.x * this.x + this.y * this.y + this.z * this.z);
  }

  normalize() {
    let mag = this.magnitude();
    this.x /= mag;
    this.y /= mag;
    this.z /= mag;

    return this;
  }

  multiply(other) {
    if(typeof other === 'number') {
      this.x *= other;
      this.y *= other;
      this.z *= other;
    } else {
      this.x = other.elements[0] * this.x + other.elements[4] * this.y + other.elements[8] * this.z + other.elements[12] * this.w;
      this.y = other.elements[1] * this.x + other.elements[5] * this.y + other.elements[9] * this.z + other.elements[13] * this.w;
      this.z = other.elements[2] * this.x + other.elements[6] * this.y + other.elements[10] * this.z + other.elements[14] * this.w;
    }

    return this;
  }
}
