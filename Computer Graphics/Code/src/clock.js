const VSHADER_SOURCE = `
attribute vec4 a_Position;
uniform mat4 u_xformMatrix;
void main() {
    gl_Position = u_xformMatrix * a_Position;
}`;

const FSHADER_SOURCE = `
void main() {
    gl_FragColor = vec4(1.0, 1.0, 1.0, 1.0);
}`;

function createCircle(numPoints) {
    const totalRadians = 2 * Math.PI;
    const radian = totalRadians / numPoints;
    const pointArr = [1.0, 0.0];
    let currentRadians = radian;
    while (currentRadians < totalRadians) {
        pointArr.push(Math.cos(currentRadians), Math.sin(currentRadians));
        currentRadians += radian;
    }
    return pointArr;
}

function createHand() {
    return [0.0, 0.0, 0.33, 0.33, 1.0, 0.0, 0.0, 0.0, 0.33, -0.33];
}

function createHourHand() {
    return [0.0, 0.33, 1.0, 0.33, 0.0, -0.33, 1.0, -0.33];
}

const gl = getAndClear();
initShaders(gl, VSHADER_SOURCE, FSHADER_SOURCE);
const u_xformMatrix = gl.getUniformLocation(gl.program, 'u_xformMatrix');

setInterval(() => {
    //Clear the screan
    gl.clear(gl.COLOR_BUFFER_BIT);
    //Make the transformation matrix
    const transformation = new Matrix4();
    //Draw outer circle
    initVertexBuffers(gl, createCircle(50));
    gl.uniformMatrix4fv(u_xformMatrix, false, transformation.setIdentity().elements);
    gl.drawArrays(gl.LINE_LOOP, 0, 50);

    //Draw inner circle
    initVertexBuffers(gl, createCircle(20));
    transformation.setScale(0.05, 0.05, 1);
    gl.uniformMatrix4fv(u_xformMatrix, false, transformation.elements);
    gl.drawArrays(gl.LINE_LOOP, 0, 20);

    //Draw marks
    for (let i = 0; i < 12; i++) {
        transformation.setRotate(30 * i, 0, 0, 1);
        transformation.translate(0.9, 0, 0);
        transformation.scale(0.1, 0.05, 1);
        initVertexBuffers(gl, createHourHand());
        gl.uniformMatrix4fv(u_xformMatrix, false, transformation.elements);
        gl.drawArrays(gl.LINES, 0, 4);
    }

    //Draw hand
    transformation.setRotate(-6 * (new Date()).getSeconds() + 90, 0, 0, 1);
    transformation.scale(0.6, 0.2, 1);
    initVertexBuffers(gl, createHand());
    gl.uniformMatrix4fv(u_xformMatrix, false, transformation.elements);
    gl.drawArrays(gl.TRIANGLE_STRIP, 0, 5);
}, 1000);