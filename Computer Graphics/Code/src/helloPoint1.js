gl = getAndClear();

const vSHADER = `
  void main() {
    gl_Position = vec4(0.5, -0.5, 0.0, 1.0);
    gl_PointSize = 10.0;
  }`;

const fSHADER = `
  void main() {
    gl_FragColor = vec4(1.0, 1.0, 0.0, 1.0);
  }`;

initShaders(gl, vSHADER, fSHADER);
gl.drawArrays(gl.POINTS, 0, 1);
