/*************************
 *    Jonathan Troyer    *
 *************************/
const vShader = `
  attribute vec4 a_Position;
  uniform mat4 u_xformMatrix;
  void main() {
    gl_Position = u_xformMatrix * a_Position;
  }`;

const fShader = `
  void main() {
    gl_FragColor = vec4(1.0, 0.0, 0.0, 1.0);
  }`;

const gl = getAndClear();
initShaders(gl, vShader, fShader);
initVertexBuffers(gl, new Float32Array([
   0.0,  0.5,
  -0.5, -0.5,
   0.5, -0.5,
   0.0,  1.0,
   0.0, -1.0,
  -1.0,  0.0,
   1.0,  0.0
]));

const ANGLE = 45.0;

const radian = Math.PI * ANGLE / 180.0;
var cosB = Math.cos(radian),
    sinB = Math.sin(radian);

const rotationMatrix = new Float32Array([
   cosB, sinB, 0.0, 0.0,
  -sinB, cosB, 0.0, 0.0,
    0.0,  0.0, 1.0, 0.0,
    0.0,  0.0, 0.0, 1.0
]);

const identityMatrix = new Float32Array([
  1.0, 0.0, 0.0, 0.0,
  0.0, 1.0, 0.0, 0.0,
  0.0, 0.0, 1.0, 0.0,
  0.0, 0.0, 0.0, 1.0
])

// Pass the rotation matrix to the vertex shader
var u_xformMatrix = gl.getUniformLocation(gl.program, 'u_xformMatrix');
gl.uniformMatrix4fv(u_xformMatrix, false, rotationMatrix);
gl.drawArrays(gl.TRIANGLES, 0, 3);
gl.uniformMatrix4fv(u_xformMatrix, false, identityMatrix);
gl.drawArrays(gl.LINES, 3, 4);
