/*******************
 * Jonathan Troyer *
 * Program 1       *
 *******************/

const gl = getAndClear();
loadShaders(gl, 'src/ColoredPoints/coloredPoints.vert', 'src/ColoredPoints/coloredPoints.frag').then(() => {
    const a_Position = gl.getAttribLocation(gl.program, 'a_Position');
    const u_FragColor = gl.getUniformLocation(gl.program, 'u_FragColor');

    const canvas = document.getElementById('canvas');
    let g_points = [];
    let g_colors = [];
    canvas.onmousedown = ev => {
      let x = ev.clientX;
      let y = ev.clientY;
      const rect = ev.target.getBoundingClientRect();

      x = ((x - rect.left) - canvas.width / 2) / (canvas.width / 2);
      y = (canvas.height / 2 - (y - rect.top)) / (canvas.height / 2);

      g_points.push([x, y]);
      if (x >= 0.0 && y >= 0.0) {
          g_colors.push([1.0, 0.0, 0.0, 1.0]);
      } else if (x < 0.0 && y >= 0.0) {
          g_colors.push([0.0, 0.0, 1.0, 1.0]);
      } else if (x < 0.0 && y < 0.0) {
          g_colors.push([0.0, 1.0, 0.0, 1.0]);
      } else {
          g_colors.push([1.0, 1.0, 1.0, 1.0]);
      }

      gl.clear(gl.COLOR_BUFFER_BIT);

      const len = g_points.length;
      for (let i = 0; i < len; i++) {
          const xy = g_points[i];
          const rgba = g_colors[i];

          gl.vertexAttrib3f(a_Position, xy[0], xy[1], 0.0);
          gl.uniform4f(u_FragColor, rgba[0], rgba[1], rgba[2], rgba[3]);

          gl.drawArrays(gl.POINTS, 0, 1);
      }
    };
});
