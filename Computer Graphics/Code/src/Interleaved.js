const VSHADER_SOURCE = `
  attribute float a_Size;
  attribute vec4 a_Position;
  attribute vec4 a_Color;
  varying vec4 v_Color;
  void main() {
    gl_Position = a_Position;
    gl_PointSize = a_Size;
    v_Color = a_Color;
  }`;

const FSHADER_SOURCE = `
  precision mediump float;
  varying vec4 v_Color;
  void main() {
    gl_FragColor = v_Color;
  }`;

const gl = getAndClear();
initShaders(gl, VSHADER_SOURCE, FSHADER_SOURCE);

//Interleaved position, color, and size buffers
const interleaved = new Float32Array([
  //Points     Colors        Sizes
   0.0,  0.5, 1.0, 0.0, 0.0, 10,
  -0.5, -0.5, 0.0, 1.0, 0.0, 20,
   0.5, -0.5, 0.0, 0.0, 1.0, 30
]);
const FSIZE = interleaved.BYTES_PER_ELEMENT;
const numPoints = 3;

//Setup buffer
const interleavedBuffer = gl.createBuffer();
gl.bindBuffer(gl.ARRAY_BUFFER, interleavedBuffer);
gl.bufferData(gl.ARRAY_BUFFER, interleaved, gl.STATIC_DRAW);

//Add position
const a_Position = gl.getAttribLocation(gl.program, 'a_Position');
gl.vertexAttribPointer(a_Position, 2, gl.FLOAT, false, FSIZE * 6, 0);
gl.enableVertexAttribArray(a_Position);

//Add color
const a_Color = gl.getAttribLocation(gl.program, 'a_Color');
gl.vertexAttribPointer(a_Color, 3, gl.FLOAT, false, FSIZE * 6, FSIZE * 2);
gl.enableVertexAttribArray(a_Color);

//Add size
const a_Size = gl.getAttribLocation(gl.program, 'a_Size');
gl.vertexAttribPointer(a_Size, 1, gl.FLOAT, false, FSIZE * 6, FSIZE * 5);
gl.enableVertexAttribArray(a_Size);

//Draw
gl.drawArrays(gl.TRIANGLES, 0, numPoints);
