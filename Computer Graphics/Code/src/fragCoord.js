const VSHADER_SOURCE = `
  attribute vec4 a_Position;
  uniform mat4 u_xformMatrix;
  void main() {
    gl_Position = u_xformMatrix * a_Position;
  }`;

const FSHADER_SOURCE = `
  precision mediump float;
  uniform float u_Height;
  void main() {
    gl_FragColor = vec4(0.0, 1.0-gl_FragCoord.y/u_Height, 0.0, 1.0);
  }`;

const ANGLE_STEP = 45.0;

const gl = getAndClear();
initShaders(gl, VSHADER_SOURCE, FSHADER_SOURCE);

let currentAngle = 0.0;
const modelMatrix = new Matrix4();
const vertices = new Float32Array([
  0, 0.5, -0.5, -0.5, 0.5, -0.5
]);

const vertexBuffer = gl.createBuffer();
gl.bindBuffer(gl.ARRAY_BUFFER, vertexBuffer);
gl.bufferData(gl.ARRAY_BUFFER, vertices, gl.STATIC_DRAW);

const a_Position = gl.getAttribLocation(gl.program, 'a_Position');
gl.vertexAttribPointer(a_Position, 2, gl.FLOAT, false, 0, 0);
gl.enableVertexAttribArray(a_Position);
const u_ModelMatrix = gl.getUniformLocation(gl.program, 'u_xformMatrix');
gl.uniformMatrix4fv(u_ModelMatrix, false, modelMatrix.elements);

const u_Height = gl.getUniformLocation(gl.program, 'u_Height');
gl.uniform1f(u_Height, gl.drawingBufferHeight);

gl.drawArrays(gl.TRIANGLES, 0, 3);

let tickLast = Date.now();
function tick() {
  let now = Date.now();
  let elapsed = now - tickLast;
  tickLast = now;
  currentAngle = currentAngle + (ANGLE_STEP * elapsed) / 1000.0;
  modelMatrix.setRotate(currentAngle, 0, 0, 1);
  gl.uniformMatrix4fv(u_ModelMatrix, false, modelMatrix.elements);
  gl.clear(gl.COLOR_BUFFER_BIT);
  gl.drawArrays(gl.TRIANGLES, 0, 3);
  window.requestAnimationFrame(tick);
}

tick();
