const VSHADER_SOURCE = `
    attribute vec4 a_Position;
    void main() {
      gl_Position = a_Position;
    }`;

const FSHADER_SOURCE = `
    void main() {
      gl_FragColor = vec4(1.0, 0.0, 0.0, 1.0);
    }`;

const gl = getAndClear();
initShaders(gl, VSHADER_SOURCE, FSHADER_SOURCE);

async function main() {
  const dinoText = await (await fetch('http://10.51.20.24/dino.txt')).text();
  const textLines = dinoText.split("\n");

  let maxX = 0, maxY = 0;
  let lines = [];

  for (let i = 0; i < textLines.length; i += parseInt(textLines[i]) + 1) {
    let line = [];
    for (let j = i + 1; j <= parseInt(textLines[i]) + i; j++) {
      let p = textLines[j].split(" ");
      p[0] = parseInt(p[0]);
      p[1] = parseInt(p[1]);

      if (p[0] > maxX)
        maxX = p[0];
      if (p[1] > maxY)
        maxY = p[1];

      line = line.concat(p);
    }
    lines.push(line);
  }
  lines = lines.map((line) => line.map((point, index) => scale (point, 0, ((index % 2 === 0) ? maxX : maxY), -1, 1)));
  let lineLengths = [];
  const linesFlattened = [];
  for (const line of lines) {
    linesFlattened = linesFlattened.concat(line);
    lineLengths.push(line.length / 2);

    const points = new Float32Array(line);
    const n = line.length / 2;

    const vertexBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vertexBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, points, gl.STATIC_DRAW);

    const a_Position = gl.getAttribLocation(gl.program, 'a_Position');
    gl.vertexAttribPointer(a_Position, 2, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(a_Position);

    gl.drawArrays(gl.LINE_STRIP, 0, n);
  }
}

main();
