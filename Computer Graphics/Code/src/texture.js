const VSHADER_SOURCE = `
  uniform mat4 u_TransformMatrix;
  attribute vec4 a_Position;
  attribute vec2 a_TexCoord;
  varying vec2 v_TexCoord;
  void main() {
    gl_Position = u_TransformMatrix * a_Position;
    v_TexCoord = a_TexCoord;
  }`;

const FSHADER_SOURCE =`
  precision mediump float;
  uniform sampler2D u_Sampler;
  varying vec2 v_TexCoord;
  void main() {
    gl_FragColor = texture2D(u_Sampler, v_TexCoord);
  }`;

const canvas = document.getElementById('canvas');
const gl = canvas.getContext('webgl');
gl.clearColor(0.0, 0.0, 0.0, 1.0);
gl.clear(gl.COLOR_BUFFER_BIT);
initShaders(gl, VSHADER_SOURCE, FSHADER_SOURCE)

const n = initVertexBuffers(gl);
initTextures(gl, n);

const u_TransformMatrix = gl.getUniformLocation(gl.program, 'u_TransformMatrix');
let angle = 0.0;
const transform = new Matrix4();

let time = new Date();
function animate() {
  let now = new Date();
  angle += (45 * (now-time) / 1000.0);
  angle = angle % 360;
  time = now;
  transform.setRotate(angle, 0, 0, 1);
  gl.uniformMatrix4fv(u_TransformMatrix, false, transform.elements);
  gl.clear(gl.COLOR_BUFFER_BIT);
  gl.drawArrays(gl.TRIANGLE_STRIP, 0, n);
  requestAnimationFrame(animate, canvas);
}
animate();


function initVertexBuffers(gl) {
  var verticesTexCoords = new Float32Array([
    // Vertex coordinates, texture coordinate
    -0.9, -0.9,   0.0, 0.0,
     0.0,  0.9,   0.5, 1.0,
     0.9, -0.9,   1.0, 0.0  
  ]);
  var n = 3; // The number of vertices

  // Create the buffer object
  var vertexTexCoordBuffer = gl.createBuffer();

  // Bind the buffer object to target
  gl.bindBuffer(gl.ARRAY_BUFFER, vertexTexCoordBuffer);
  gl.bufferData(gl.ARRAY_BUFFER, verticesTexCoords, gl.STATIC_DRAW);

  var FSIZE = verticesTexCoords.BYTES_PER_ELEMENT;
  //Get the storage location of a_Position, assign and enable buffer
  var a_Position = gl.getAttribLocation(gl.program, 'a_Position');
  gl.vertexAttribPointer(a_Position, 2, gl.FLOAT, false, FSIZE * 4, 0);
  gl.enableVertexAttribArray(a_Position);  // Enable the assignment of the buffer object

  // Get the storage location of a_TexCoord
  var a_TexCoord = gl.getAttribLocation(gl.program, 'a_TexCoord');
  // Assign the buffer object to a_TexCoord variable
  gl.vertexAttribPointer(a_TexCoord, 2, gl.FLOAT, false, FSIZE * 4, FSIZE * 2);
  gl.enableVertexAttribArray(a_TexCoord);  // Enable the assignment of the buffer object

  return n;
}

async function initTextures(gl, n) {
  const texture = gl.createTexture();
  const u_Sampler = gl.getUniformLocation(gl.program, 'u_Sampler');
  const image = new Image();
  image.onload = function() {
    loadTexture(gl, n, texture, u_Sampler, image);
  }
  image.crossOrigin = "Anonymous";
  image.src = 'http://10.51.20.24/parasol.jpg';
}

function loadTexture(gl, n, texture, u_Sampler, image) {
  gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, 1); // Flip the image's y axis
  // Enable texture unit0
  gl.activeTexture(gl.TEXTURE0);
  // Bind the texture object to the target
  gl.bindTexture(gl.TEXTURE_2D, texture);

  // Set the texture parameters
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
  // Set the texture image
  gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGB, gl.RGB, gl.UNSIGNED_BYTE, image);
  
  // Set the texture unit 0 to the sampler
  gl.uniform1i(u_Sampler, 0);
}
