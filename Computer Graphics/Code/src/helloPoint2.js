const gl = getAndClear();

const vSHADER = `
  attribute vec4 a_Position;
  attribute float a_Size;
  void main() {
    gl_Position = a_Position;
    gl_PointSize = a_Size;
  }`;

const fSHADER = `
  void main() {
    gl_FragColor = vec4(1.0, 0.0, 0.0, 1.0);
  }`;

initShaders(gl, vSHADER, fSHADER);
const a_Position = gl.getAttribLocation(gl.program, 'a_Position');
const a_Size = gl.getAttribLocation(gl.program, 'a_Size');
gl.vertexAttrib3f(a_Position, 0.0, 0.0, 0.0);
gl.vertexAttrib1f(a_Size, 10.0);
gl.drawArrays(gl.POINTS, 0, 1);
