//  Jonathan Troyer

//Define a new Vue app
let app = new Vue({
	//Bind it to the element with id app
	el: '#app',
	data() {
		//Define our variables, which I think speak for themselves
		return {
			perspective: true,
			fov: 90,
			// 16:9 aspect ratio
			aspectRatio: 1.77777777,
			ball: new Float32Array([
				//Bottom
				0.50, 0.00, -0.50,	1.0,1.0,1.0, //  0
				//Bottom ring
				0.25, 0.25, -0.25,	1.0,0.0,0.0, //  1
				0.25, 0.25, -0.75,	1.0,0.0,0.0, //  2
				0.75, 0.25, -0.25,	1.0,0.0,0.0, //  3
				0.75, 0.25, -0.75,	1.0,0.0,0.0, //  4
				//Middle ring
				0.00, 0.50, -0.50,	0.0,1.0,0.0, //  5
				0.125, 0.50, -0.25,	0.0,1.0,0.0, //  6
				0.125, 0.50, -0.75,	0.0,1.0,0.0, //  7
				0.5, 0.50,  0.00,	0.0,1.0,0.0, //  8
				0.5, 0.50, -1.00,	0.0,1.0,0.0, //  9
				0.875, 0.50, -0.25,	0.0,1.0,0.0, // 10
				0.875, 0.50, -0.75,	0.0,1.0,0.0, // 11
				1.00, 0.50, -0.50,	0.0,1.0,0.0, // 12
				//Top ring
				0.25, 0.75, -0.25,	0.0,0.0,1.0, // 13
				0.25, 0.75, -0.75,	0.0,0.0,1.0, // 14
				0.75, 0.75, -0.25,	0.0,0.0,1.0, // 15
				0.75, 0.75, -0.75,	0.0,0.0,1.0, // 16
				//Top
				0.50, 1.00, -0.50,	1.0,1.0,1.0  // 17
			]),
			//Indecies for the elements of the ball, definitely the hardest thing I did
			ballIndices: new Uint8Array([
				//Bottom
				0,1,2, 0,2,4, 0,4,3, 0,3,1,
				//Bottom ring
				1,5,6, 1,6,8, 1,8,3,
				3,8,10, 3,10,12, 3,12,4,
				4,12,11, 4,11,9, 4,9,2,
				2,9,7, 2,7,5, 2,5,1,
				//Top ring
				13,5,6, 13,6,8, 13,8,15,
				15,8,10, 15,10,12, 15,12,16,
				16,12,11, 16,11,9, 16,9,14,
				14,9,7, 14,7,5, 14,5,13,
				//Top
				17,13,14, 17,14,16, 17,16,15, 17,15,13
			]),
			pyramid: new Float32Array([
				0.5, 1.0, -0.5,		0.5, 1.0,
				0.0, 0.0,  0.0,		0.0, 0.0,
				1.0, 0.0,  0.0,		1.0, 0.0,
				1.0, 0.0, -1.0,		0.0, 0.0,
				0.0, 0.0, -1.0,		1.0, 0.0,
				0.0, 0.0,  0.0,		0.0, 0.0
			]),
			gl: getAndClear(),
			eye: {
				x: 0,
				y: 1,
				z: 2
			},
			lookAt: {
				x: 0,
				y: 0,
				z: 0
			},
			vShader: `
				attribute vec4 aPosition;
				attribute vec2 aTexCoord;
				attribute vec4 aColor;
				uniform mat4 uMVPMatrix;
				varying vec2 vTexCoord;
				varying vec4 vColor;
				void main() {
					gl_Position = uMVPMatrix * aPosition;
					gl_PointSize = 2.0;
					vColor = aColor;
					vTexCoord = aTexCoord;

				}`,
			fShader: `
				precision mediump float;
				uniform sampler2D uSampler;
				uniform bool uColor;
				varying vec2 vTexCoord;
				varying vec4 vColor;
				void main() {
					if(uColor) {
						gl_FragColor = vColor;
					} else {
						gl_FragColor = texture2D(uSampler, vTexCoord);
					}
				}`,
			uMVPMatrix: undefined,
			uColor: undefined,
			aPosition: undefined,
			aTexCoord: undefined,
			aColor: undefined,
			angle: 0,
			angleStep: 45,
			time: new Date(),
			pause: true,
			type: 'pyramid',
			//Initially I was gonna make a random formula but these things have meaning now so I should rename them
			//Problem is, refactoring JS is hard and I'm lazy. Plus it adds to the 'mystique'
			a: 1,
			b: 1,
			c: 1,
			d: 1,
			e: 1,
			iterations: 25,
			FSIZE: 4
		}
	},
	//Define computed variables
	//These can be used like variables and update automatically whenever one of their dependencies change
	computed: {
		// Updates whenever eye or lookAt xyz changes
		viewMatrix() {
			let mat = new Matrix4();
			mat.setLookAt(
				this.eye.x, this.eye.y, this.eye.z,
				this.lookAt.x, this.lookAt.y, this.lookAt.z,
				0, 1, 0
			);
			return mat;	
		},
		projMatrix() {
			//Updates whenever perspective, fov, or aspectRatio changes
			let mat = new Matrix4();
			if(this.perspective) {
				mat.setPerspective(this.fov, this.aspectRatio, 1, 100);
			} else {
				mat.setOrtho(-1.0, 1.0, -1.0, 1.0, 1, 100);
			}
			return mat;
		},
		modelMatrix() {
			let mat = new Matrix4();
			//Always shift the object back 5 for your viewing pleasure
			mat.setTranslate(0,0,-5);
			//Rotate by the angle, wil update on this
			mat.setRotate(this.angle, 0, 1, 0);
			if(this.type != '???'){
				//I've defined my shapes in 0-1 space, so center them
				mat.translate(-0.5,0,0.5);
			}
			else{
				//The thing is defiend in size by a and e (and update on changes of those)
				mat.translate(-0.5*this.a, 0, 0.5*this.e);
			}
			return mat;
		},
		//Will update whenever view, proj, or model matrices update
		mvpMatrix() {
			let mat = new Matrix4(this.projMatrix);
			mat.multiply(this.viewMatrix).multiply(this.modelMatrix);
			return mat;
		},
		//The sine wave thing
		thing() {
			//Make a new array for the points
			//It's a iterations x iterations 2D array, with 6 values per point (xyz, rgb)
			//I add one so that there's never a blank array (i.e. 0 iterations just creates a flat thing)
			let buff = new Float32Array(Math.pow(this.iterations+1, 2) * 6);

			const yStep = (360 / this.iterations) * Math.PI / 180;
			const xStep = 1 / this.iterations;
			let pos = 0;
			for(let i = 0; i <= this.iterations; i++) {
				const x = i * xStep * this.a;
				for(let k = 0; k <= this.iterations; k++) {
					const y = Math.sin(i * yStep * this.b) * Math.sin(k * yStep * this.c) * this.d;
					const absY = Math.abs(y) * 2;
					const z = k * xStep * this.e;
					//Position
					buff[pos] = x; buff[pos+1] = y; buff[pos+2] = -z;
					//Color
					buff[pos+3] = x*z; buff[pos+4] = absY; buff[pos+5] = 1 - absY;
					pos+=6;
				}
			}

			return buff;
		}
	},
	//These are functions that can be used within the Vue component scope
	methods: {
		//Draws the buffer, depending on the type
		//Yes I know this is a bit janky
		draw() {
			//Set the MVP matrix
			this.gl.uniformMatrix4fv(this.uMVPMatrix, false, this.mvpMatrix.elements);
			//Clear color and depth buffers
			this.gl.clear(this.gl.COLOR_BUFFER_BIT | this.gl.DEPTH_BUFFER_BIT);

			if(this.type == 'pyramid') {
				this.gl.drawArrays(this.gl.TRIANGLE_FAN, 0, this.pyramid.length / 5);
			}
			else if(this.type == 'ball') {
				this.gl.drawElements(this.gl.TRIANGLES, this.ballIndices.length, this.gl.UNSIGNED_BYTE, 0);
			}
			else {
				this.gl.drawArrays(this.gl.POINTS, 0, this.thing.length / 6);
			}
		},
		//Update the rotation (thus triggering the model matrix to change which changes the mvp matrix) and redraw
		animate() {
			this.angle += this.angleStep * ((new Date()) - this.time) / 1000;
			this.time = new Date();
			if(this.angle >=360) {
				this.angle = this.angle % 360;
			}
			this.draw();
			if(!this.pause) {
				//If we're not paused, reanimate every frame
				window.requestAnimationFrame(this.animate);
			}
		},
		initVertexBuffers() {
			//Unbind any old buffers. I don't think I need this but it's staying until I'm sure I don't
			this.gl.bindBuffer(this.gl.ARRAY_BUFFER, null);

			//Make a new buffer and bind it
			let buffer = this.gl.createBuffer();
			this.gl.bindBuffer(this.gl.ARRAY_BUFFER, buffer);

			//The pyramid buffer is points and textures, so it's set up differently
			if(this.type == 'pyramid') {
				//Disable the color var
 		 		this.gl.disableVertexAttribArray(this.aColor);
 		 		//Set the bool for the fragment shader. Yes I know it's more proper to swap shaders but we haven't done that yet even though it's easy
 		 		this.gl.uniform1f(this.uColor, 0);

 		 		//Get the data for the pyramid and shove it in the buffer
				this.gl.bufferData(this.gl.ARRAY_BUFFER, this.pyramid, this.gl.STATIC_DRAW);
				//Setup vertices
				this.gl.vertexAttribPointer(this.aPosition, 3, this.gl.FLOAT, false, this.FSIZE*5, 0);
				//Setup texels
  				this.gl.vertexAttribPointer(this.aTexCoord, 2, this.gl.FLOAT, false, this.FSIZE * 5, this.FSIZE * 3);
  				//Enable the texel var
 		 		this.gl.enableVertexAttribArray(this.aTexCoord);
			}
			else {
				//Disable the texture var
 		 		this.gl.disableVertexAttribArray(this.aTexCoord);
 		 		//Set the bool for the frag shader
				this.gl.uniform1f(this.uColor, 1);

				//Both the ball and the thing use interleaved xyx/rgb data, so set it up as such
				this.gl.vertexAttribPointer(this.aPosition, 3, this.gl.FLOAT, false, this.FSIZE*6, 0);

				this.gl.vertexAttribPointer(this.aColor, 3, this.gl.FLOAT, false, this.FSIZE*6, this.FSIZE*3);
				this.gl.enableVertexAttribArray(this.aColor);

				if(this.type == 'ball') {
					this.gl.bufferData(this.gl.ARRAY_BUFFER, this.ball, this.gl.STATIC_DRAW);

					//Create a second buffer for the elements
					let indicesBuffer = this.gl.createBuffer();
					this.gl.bindBuffer(this.gl.ELEMENT_ARRAY_BUFFER, indicesBuffer);
  					this.gl.bufferData(this.gl.ELEMENT_ARRAY_BUFFER, this.ballIndices, this.gl.STATIC_DRAW);
				} else {
					this.gl.bufferData(this.gl.ARRAY_BUFFER, this.thing, this.gl.STATIC_DRAW);
				}
			}
			//In either case, we want to enable the position var, because, like, that's the point...
			this.gl.enableVertexAttribArray(this.aPosition);

			//Unbind the old buffer
			this.gl.bindBuffer(this.gl.ARRAY_BUFFER, null);
		}
	},
	//Runs when the Vue component is created
	created() {
		//Initialize the shaders... like it says
		initShaders(this.gl, this.vShader, this.fShader);
		//Enable the depth buffer. For a while I was wondering why I could see through my pyramid until I realized I was doing this
		this.gl.enable(this.gl.DEPTH_TEST);

		//Save all the shader vars as JS pointers for future use
		this.uMVPMatrix = this.gl.getUniformLocation(this.gl.program, 'uMVPMatrix');
		this.uColor = this.gl.getUniformLocation(this.gl.program, 'uColor');

		this.aPosition = this.gl.getAttribLocation(this.gl.program, 'aPosition');
		this.aTexCoord = this.gl.getAttribLocation(this.gl.program, 'aTexCoord');
		this.aColor = this.gl.getAttribLocation(this.gl.program, 'aColor');

		//See above
		this.initVertexBuffers();

		//Load the image as a texture
  		const image = new Image();
  		const ref = this;
  		image.onload = function() {
    		ref.gl.pixelStorei(ref.gl.UNPACK_FLIP_Y_WEBGL, 1);
		  	ref.gl.activeTexture(ref.gl.TEXTURE0);
		  	ref.gl.bindTexture(ref.gl.TEXTURE_2D, ref.gl.createTexture());

		  	ref.gl.texParameteri(ref.gl.TEXTURE_2D, ref.gl.TEXTURE_MIN_FILTER, ref.gl.NEAREST);
		  	ref.gl.texImage2D(ref.gl.TEXTURE_2D, 0, ref.gl.RGB, ref.gl.RGB, ref.gl.UNSIGNED_BYTE, image);
		  
		  	ref.gl.uniform1i(ref.gl.getUniformLocation(ref.gl.program, 'uSampler'), 0);

    		ref.animate();
  		}
  		image.crossOrigin = "Anonymous";
  		//I uploaded the example image with the flower to imgur so I didn't have to host it myself or get it from the filesystem or other jank
  		image.src = 'https://i.imgur.com/tNazo75.jpg';
	},
	//Watches a variable and calls the function whenever the variable changes
	//We draw only when not animating rather than always drawing so that we can save a draw
	watch: {
		//Because the MVP matrix changes whenever one of the sub-matrices change, we only need to redraw when this one changes
		mvpMatrix: function(newMVPM) {
			if(this.pause)
				this.draw();
		},
		pause: function(newPause) {
			//Set the new time to now so that we don't jump from the last point
			this.time = new Date();
			this.animate();
		},
		type: function(newType) {
			//If what we're drawing changes, change the buffers to what we're drawing
			this.initVertexBuffers();
			if(this.pause)
				this.draw();
		},
		thing: function(newThing) {
			//Yeah you need to change the buffer data whenever the thing changes, because it's changing
			this.initVertexBuffers();
			if(this.pause)
				this.draw();
		}
	}
})