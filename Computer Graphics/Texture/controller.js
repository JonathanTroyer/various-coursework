/*************************
 *    Jonathan Troyer    *
 *************************/

// language=GLSL
const vShader = `
    attribute vec4 aPosition;
    attribute vec2 aTexCoord;
    uniform mat4 uTransform;
    varying vec2 vTexCoord;
    void main() {
        gl_Position = uTransform * aPosition;
        vTexCoord = aTexCoord;
    }`;

// language=GLSL
const fShader = `
    precision mediump float;
    varying vec2 vTexCoord;
    void main() {
        //Get even/odd of x/y coord
        //If they're the same, the difference will be 0
        //If they're different, the difference will be 1 (abs taken to make positive for white)
        float val = abs(floor(mod(vTexCoord.x * 10.0, 2.0))-floor(mod(vTexCoord.y * 10.0, 2.0)));
        //A var is used to reduce cycles
        gl_FragColor = vec4(1.0, val, val, 1.0);
    }
`;

const quad = new Float32Array([
    -0.5, -0.5, 0.0, 0.0,
    -0.5, 0.5, 0.0, 1.0,
    0.5, 0.5, 1.0, 1.0,
    0.5, -0.5, 1.0, 0.0
]);

const tri = new Float32Array([
    -0.5, -0.5, 0.0, 0.0,
    0.0, 0.5, 0.5, 1.0,
    0.5, -0.5, 1.0, 0.0
]);

const FSIZE = quad.BYTES_PER_ELEMENT;
let drawTri = true;
let animate = true;

const gl = getAndClear();
initShaders(gl, vShader, fShader);

const vertexBuffer = gl.createBuffer();
gl.bindBuffer(gl.ARRAY_BUFFER, vertexBuffer);

const aPosition = gl.getAttribLocation(gl.program, 'aPosition');
gl.vertexAttribPointer(aPosition, 2, gl.FLOAT, false, FSIZE * 4, 0);
gl.enableVertexAttribArray(aPosition);

const aTexCoord = gl.getAttribLocation(gl.program, 'aTexCoord');
gl.vertexAttribPointer(aTexCoord, 2, gl.FLOAT, false, FSIZE * 4, FSIZE * 2);
gl.enableVertexAttribArray(aTexCoord);

let last = new Date();
let currentAngle = 0;
const transform = new Matrix4();
const uTransform = gl.getUniformLocation(gl.program, 'uTransform');

function tick() {
    if (animate) {
        let now = new Date();
        currentAngle = ((90 * (now - last)) / 1000 + currentAngle) % 360;
        last = now;
        transform.setRotate(currentAngle, 0, 1, 0);
        gl.uniformMatrix4fv(uTransform, false, transform.elements);
        draw();
        requestAnimationFrame(tick);
    } else {
        resetTransform();
        draw();
    }
}

function draw() {
    gl.clear(gl.COLOR_BUFFER_BIT);
    gl.drawArrays(gl.TRIANGLE_FAN, 0, (drawTri ? tri : quad).length / 4);
}

function resetTransform() {
    transform.setIdentity();
    gl.uniformMatrix4fv(uTransform, false, transform.elements);
}

const eDrawTri = document.getElementById("drawTri");
const eAnimate = document.getElementById("animate");

function update() {
    drawTri = eDrawTri.checked;
    animate = eAnimate.checked;
    gl.bufferData(gl.ARRAY_BUFFER, drawTri ? tri : quad, gl.STATIC_DRAW);
    tick();
}

update();
