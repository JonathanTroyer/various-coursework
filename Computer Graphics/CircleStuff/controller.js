/*************************
 *    Jonathan Troyer    *
 *************************/

function getPoints(numPoints) {
  const totalRadians = 2 * Math.PI;
  const radian = totalRadians / numPoints;
  const pointArr = [1.0, 0.0];
  let currentRadians = radian;
  while (currentRadians < totalRadians) {
    pointArr.push(Math.cos(currentRadians), Math.sin(currentRadians));
    currentRadians += radian;
  }
  return pointArr;
}

let numPoints = 10;

function drawCircle() {
  if (numPoints < 3)
    numPoints = 3;

  let points = getPoints(numPoints);
  const filled = document.getElementById('filled').checked;
  if (filled) {
    points.unshift(0.0, 0.0);
    points.push(1.0, 0.0);
  }

  initVertexBuffers(gl, points);

  gl.clear(gl.COLOR_BUFFER_BIT);
  if (filled)
    gl.drawArrays(gl.TRIANGLE_FAN, 0, numPoints+2);
  else
    gl.drawArrays(gl.LINE_LOOP, 0, numPoints);
}

// language=GLSL
const VSHADER_SOURCE = `
    attribute vec4 a_Position;
    void main() {
      gl_Position = a_Position * vec4(1.0, 1.0, 1.0, 1.0);
    }`;

// language=GLSL
const FSHADER_SOURCE = `
    void main() {
      gl_FragColor = vec4(0.0, 1.0, 1.0, 1.0);
    }`;

const gl = getAndClear();
initShaders(gl, VSHADER_SOURCE, FSHADER_SOURCE);
drawCircle();
