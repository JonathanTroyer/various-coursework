function getAndClear(color = [0.0, 0.0, 0.0, 1.0]) {
    let canvas = document.getElementById('canvas');
    let gl = canvas.getContext('webgl');
    gl.clearColor(...color);
    gl.clear(gl.COLOR_BUFFER_BIT);
    return gl;
}

function normalize(val, max, min = 0) {
    return (val - min) / (max - min);
}

function scale(val, max, min, nMax, nMin) {
    return nMin + (nMax - nMin) * (val - min) / (max - min);
}

async function loadShaders(gl, vShaderPath, fShaderPath) {
    const vShader = await(await fetch(vShaderPath)).text();
    const fShader = await (await fetch(fShaderPath)).text();
    initShaders(gl, vShader, fShader);
}

function initVertexBuffers(gl, vertices) {
  const vertexBuffer = gl.createBuffer();

  gl.bindBuffer(gl.ARRAY_BUFFER, vertexBuffer);
  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertices), gl.STATIC_DRAW);

  var a_Position = gl.getAttribLocation(gl.program, 'a_Position');
  gl.vertexAttribPointer(a_Position, 2, gl.FLOAT, false, 0, 0);
  gl.enableVertexAttribArray(a_Position);
}

/****************************
 *        cuon-utils        *
 *****************************/

// cuon-utils.js (c) 2012 kanda and matsuda
/**
 * Create a program object and make current
 * @param gl GL context
 * @param vShader a vertex shader program (string)
 * @param fShader a fragment shader program (string)
 * @return boolean if the program object was created and successfully made current
 */
function initShaders(gl, vShader, fShader) {
    const program = createProgram(gl, vShader, fShader);
    if (!program) {
        console.error('Failed to create program');
        return false;
    }

    gl.useProgram(program);
    gl.program = program;

    return true;
}

/**
 * Create the linked program object
 * @param gl GL context
 * @param vShader a vertex shader program (string)
 * @param fShader a fragment shader program (string)
 * @return WebGLProgram program object, or null if the creation has failed
 */
function createProgram(gl, vShader, fShader) {
    // Create shader object
    const vertexShader = loadShader(gl, gl.VERTEX_SHADER, vShader);
    const fragmentShader = loadShader(gl, gl.FRAGMENT_SHADER, fShader);
    if (!vertexShader || !fragmentShader) {
        return null;
    }

    // Create a program object
    const program = gl.createProgram();
    if (!program) {
        return null;
    }

    // Attach the shader objects
    gl.attachShader(program, vertexShader);
    gl.attachShader(program, fragmentShader);

    // Link the program object
    gl.linkProgram(program);

    // Check the result of linking
    const linked = gl.getProgramParameter(program, gl.LINK_STATUS);
    if (!linked) {
        console.error('Failed to link program: ' + gl.getProgramInfoLog(program));
        gl.deleteProgram(program);
        gl.deleteShader(fragmentShader);
        gl.deleteShader(vertexShader);
        return null;
    }
    return program;
}

/**
 * Create a shader object
 * @param gl GL context
 * @param type the type of the shader object to be created
 * @param source shader program (string)
 * @return WebGLShader shader object, or null if the creation has failed.
 */
function loadShader(gl, type, source) {
    // Create shader object
    const shader = gl.createShader(type);
    if (shader == null) {
        console.error('Unable to create shader');
        return null;
    }

    // Set the shader program
    gl.shaderSource(shader, source);

    // Compile the shader
    gl.compileShader(shader);

    // Check the result of compilation
    const compiled = gl.getShaderParameter(shader, gl.COMPILE_STATUS);
    if (!compiled) {
        console.error('Failed to compile shader: ' + gl.getShaderInfoLog(shader));
        gl.deleteShader(shader);
        return null;
    }

    return shader;
}
