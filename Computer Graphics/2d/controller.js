// language=GLSL
const vShader = `
attribute vec4 aPos;
uniform mat4 uTransform;
attribute vec4 aColor;
varying vec4 vColor;
void main() {
    gl_Position = uTransform * aPos;
    vColor = aColor;
}
`;

// language=GLSL
const fShader = `
precision mediump float;
varying vec4 vColor;
void main() {
    gl_FragColor = vColor;
}
`;

function getTrapezoid(color = [1.0, 1.0, 1.0]) {
    return new Float32Array([
        -1.0, -1.0, ...color,
        -0.5,  1.0, ...color,
         1.0, -1.0, ...color,
         0.5,  1.0, ...color
    ]);
}

function getSquare(color = [1.0, 1.0, 1.0]) {
    return new Float32Array([
        -1.0,  1.0, ...color,
        -1.0, -1.0, ...color,
         1.0,  1.0, ...color,
         1.0, -1.0, ...color
    ])
}

function getGradientSquare(top = [1.0, 1.0, 1.0], bottom = [0.0, 0.0, 0.0]) {
    return new Float32Array([
        -1.0,  1.0, ...top,
        -1.0, -1.0, ...bottom,
         1.0,  1.0, ...top,
         1.0, -1.0, ...bottom
    ])
}

function getTriangle(color = [1.0, 1.0, 1.0]) {
    return new Float32Array([
        -1.0, -1.0, ...color,
         0.0, 1.0, ...color,
         1.0, -1.0, ...color
    ]);
}

function getRightTriangle(color = [1.0, 1.0, 1.0]) {
    return new Float32Array([
        -1.0,  1.0, ...color,
        -1.0, -1.0, ...color,
         1.0, -1.0, ...color
    ]);
}

function getPlus(color = [1.0, 1.0, 1.0]) {
    return new Float32Array([
         0.0,  1.0, ...color,
         0.0, -1.0, ...color,
        -1.0,  0.0, ...color,
         1.0,  0.0, ...color
    ]);
}

function getBuildings(color = [1.0, 1.0, 1.0]) {
    const one = [-1.0, -1.0, ...color];
    const two = [-1.0, 0.5, ...color];
    const three = [-0.5, -0.5, ...color];
    const four = [-0.5, 1.0, ...color];
    const five = [0.5, 1.0, ...color];
    const six = [0.5, -0.5, ...color];
    const seven = [1.0, 0.5, ...color];
    const eight = [1.0, -1.0, ...color];
    return new Float32Array([
        ...one, ...two, ...three,
        ...two, ...three, ...four,
        ...three, ...four, ...five,
        ...three, ...five, ...six,
        ...five, ...six, ...seven,
        ...six, ...seven, ...eight
    ]);
}

function getCircle(numPoints, color = [1.0, 1.0, 1.0]) {
    const totalRadians = 2 * Math.PI;
    const radian = totalRadians / numPoints;
    const pointArr = [1.0, 0.0, ...color];
    let currentRadians = radian;
    while (currentRadians < totalRadians) {
        pointArr.push(Math.cos(currentRadians), Math.sin(currentRadians));
        pointArr.push(...color);
        currentRadians += radian;
    }
    return pointArr;
}

//Initialize
const gl = getAndClear([128 / 255, 174 / 255, 223 / 255, 1.0]);
initShaders(gl, vShader, fShader);
//Setup buffer
const buffer = gl.createBuffer();
gl.bindBuffer(gl.ARRAY_BUFFER, buffer);

//Setup vars
const FSIZE = 4; // 4 bytes in a 32-bit float
const transform = new Matrix4();
const GREEN = [87 / 255, 100 / 255, 54 / 255];
const BRICK = [69 / 255, 59 / 255, 68 / 255];
const LIGHT_BRICK = [80 / 255, 70 / 255, 80 / 255];
const RED_BRICK = [100/255, 70/255, 80/255];

const aPos = gl.getAttribLocation(gl.program, 'aPos');
gl.vertexAttribPointer(aPos, 2, gl.FLOAT, false, FSIZE * 5, 0);
gl.enableVertexAttribArray(aPos);

const aColor = gl.getAttribLocation(gl.program, 'aColor');
gl.vertexAttribPointer(aColor, 3, gl.FLOAT, false, FSIZE * 5, FSIZE * 2);
gl.enableVertexAttribArray(aColor);

const uTransform = gl.getUniformLocation(gl.program, 'uTransform');

function drawGrass() {
    gl.bufferData(gl.ARRAY_BUFFER, getTrapezoid(GREEN), gl.STATIC_DRAW);
    transform.setTranslate(0, -0.9, 0);
    transform.scale(1, 0.1, 1);
    gl.uniformMatrix4fv(uTransform, false, transform.elements);

    gl.drawArrays(gl.TRIANGLE_STRIP, 0, 4);
}

function drawBuildings() {
    gl.bufferData(gl.ARRAY_BUFFER, getBuildings(BRICK), gl.STATIC_DRAW);
    transform.setTranslate(0, -0.75, 0);
    transform.scale(1, 0.25, 1);
    gl.uniformMatrix4fv(uTransform, false, transform.elements);

    gl.drawArrays(gl.TRIANGLES, 0, 18);
}

function drawBase() {
    let whiteSquare = getSquare();
    let brickSquare = getGradientSquare(LIGHT_BRICK, RED_BRICK);
    let buff = new Float32Array(whiteSquare.length * 2);
    buff.set(whiteSquare);
    buff.set(brickSquare, brickSquare.length);

    gl.bufferData(gl.ARRAY_BUFFER, buff, gl.STATIC_DRAW);

    //Brick squares (sides + connector)
    transform.setTranslate(-0.2, -0.55, 0);
    transform.scale(0.1, 0.2, 1);
    gl.uniformMatrix4fv(uTransform, false, transform.elements);
    gl.drawArrays(gl.TRIANGLE_STRIP, 4, 4);

    transform.setTranslate(0.2, -0.55, 0);
    transform.scale(0.1, 0.2, 1);
    gl.uniformMatrix4fv(uTransform, false, transform.elements);
    gl.drawArrays(gl.TRIANGLE_STRIP, 4, 4);

    transform.setTranslate(0, -0.45, 0);
    transform.scale(0.1, 0.1, 1);
    gl.uniformMatrix4fv(uTransform, false, transform.elements);
    gl.drawArrays(gl.TRIANGLE_STRIP, 4, 4);

    //White squares (base + detail)
    transform.setTranslate(-0.2, -0.85, 0);
    transform.scale(0.1, 0.1, 1);
    gl.uniformMatrix4fv(uTransform, false, transform.elements);
    gl.drawArrays(gl.TRIANGLE_STRIP, 0, 4);

    transform.setTranslate(0.2, -0.85, 0);
    transform.scale(0.1, 0.1, 1);
    gl.uniformMatrix4fv(uTransform, false, transform.elements);
    gl.drawArrays(gl.TRIANGLE_STRIP, 0, 4);

    transform.setTranslate(0, -0.53, 0);
    transform.scale(0.1, 0.02, 1);
    gl.uniformMatrix4fv(uTransform, false, transform.elements);
    gl.drawArrays(gl.TRIANGLE_STRIP, 0, 4);
}

function drawBody() {
    let whiteSquare = getGradientSquare([1.0, 1.0, 1.0], [0.98, 0.98, 0.95]);
    let brickSquare = getGradientSquare(RED_BRICK, [90/255, 70/255, 80/255]);

    let buff = new Float32Array(whiteSquare.length * 2);
    buff.set(brickSquare);
    buff.set(whiteSquare, brickSquare.length);

    gl.bufferData(gl.ARRAY_BUFFER, buff, gl.STATIC_DRAW);

    transform.setTranslate(0, 0.0, 0);
    transform.scale(0.1, 0.35, 1);
    gl.uniformMatrix4fv(uTransform, false, transform.elements);
    gl.drawArrays(gl.TRIANGLE_STRIP, 0, 4);

    transform.setTranslate(-0.18, 0.0, 0);
    transform.scale(0.08, 0.35, 1);
    gl.uniformMatrix4fv(uTransform, false, transform.elements);
    gl.drawArrays(gl.TRIANGLE_STRIP, 4, 4);

    transform.setTranslate(0.18, 0.0, 0);
    transform.scale(0.08, 0.35, 1);
    gl.uniformMatrix4fv(uTransform, false, transform.elements);
    gl.drawArrays(gl.TRIANGLE_STRIP, 4, 4);
}

function drawFace() {
    let gradientSquare = getGradientSquare([0.95, 0.93, 0.93], [1.0, 1.0, 1.0]);
    let blackSquare = getSquare([0.75, 0.75, 0.75]);

    let buff = new Float32Array(blackSquare.length * 3);
    buff.set(gradientSquare);
    buff.set(blackSquare, blackSquare.length);
    buff.set(getPlus([0.75, 0.75, 0.75]), blackSquare.length * 2);
    gl.bufferData(gl.ARRAY_BUFFER, buff, gl.STATIC_DRAW);

    transform.setTranslate(0, 0.55, 0);
    transform.scale(0.2, 0.2, 1);
    gl.uniformMatrix4fv(uTransform, false, transform.elements);
    gl.drawArrays(gl.TRIANGLE_STRIP, 0, 4);

    gl.drawArrays(gl.LINE_LOOP, 4, 4);
    gl.drawArrays(gl.LINES, 8, 4);

    transform.setTranslate(0, 0.55, 0);
    transform.scale(0.2, 0.2, 1);
    transform.rotate(90, 0, 0, 1);

    gl.uniformMatrix4fv(uTransform, false, transform.elements);
    gl.drawArrays(gl.LINES, 4, 4);
}

function drawRoof() {
    let rTrangle = getRightTriangle([0.4, 0.45, 0.75]);
    let triangle = getTriangle();

    let buff = new Float32Array(triangle.length * 2);
    buff.set(triangle);
    buff.set(rTrangle, triangle.length);

    gl.bufferData(gl.ARRAY_BUFFER, buff, gl.STATIC_DRAW);

    transform.setTranslate(-0.1, 0.84, 0);
    transform.scale(0.09, 0.09, 1);
    transform.rotate(-90, 0, 0, 1);
    gl.uniformMatrix4fv(uTransform, false, transform.elements);
    gl.drawArrays(gl.TRIANGLES, 3, 3);

    transform.setTranslate(0.1, 0.84, 0);
    transform.scale(0.09, 0.09, 1);
    transform.rotate(180, 0, 0, 1);
    gl.uniformMatrix4fv(uTransform, false, transform.elements);
    gl.drawArrays(gl.TRIANGLES, 3, 3);

    transform.setTranslate(0, 0.85, 0);
    transform.scale(0.2, 0.1, 1);
    gl.uniformMatrix4fv(uTransform, false, transform.elements);
    gl.drawArrays(gl.TRIANGLES, 0, 3);
}

function drawClock() {
    let outer = getCircle(25, [0.0, 0.0, 0.0]);
    let face = getCircle(25);
    face.unshift(0.0, 0.0, 1.0, 1.0, 1.0);
    face.push(1.0, 0.0, 1.0, 1.0, 1.0);

    let buff = new Float32Array(outer.length + face.length);
    buff.set(outer);
    buff.set(face, outer.length);
    gl.bufferData(gl.ARRAY_BUFFER, buff, gl.STATIC_DRAW);

    transform.setTranslate(0, 0.55, 0);
    transform.scale(0.15, 0.15, 1);
    gl.uniformMatrix4fv(uTransform, false, transform.elements);

    gl.drawArrays(gl.LINE_LOOP, 0, outer.length/5);
    gl.drawArrays(gl.TRIANGLE_FAN, outer.length/5, face.length/5);
}

function drawHands() {
    let blackSquare = getSquare([0.0, 0.0, 0.0]);
    let redSquare = getSquare([1.0, 0.0, 0.0]);
    let buff = new Float32Array(blackSquare.length + redSquare.length);
    buff.set(blackSquare);
    buff.set(redSquare, blackSquare.length);
    gl.bufferData(gl.ARRAY_BUFFER, buff, gl.STATIC_DRAW);

    const time = new Date();

    // Hours
    transform.setTranslate(0, 0.55, 0);
    transform.rotate(-(360/12*time.getHours())%360, 0, 0, 1);
    transform.translate(0, 0.038, 0);
    transform.scale(0.01, 0.04, 0);
    gl.uniformMatrix4fv(uTransform, false, transform.elements);
    gl.drawArrays(gl.TRIANGLE_STRIP, 0, 4);

    //Minutes
    transform.setTranslate(0, 0.55, 0);
    transform.rotate(-(360/60*time.getMinutes())%360, 0, 0, 1);
    transform.translate(0, 0.048, 0);
    transform.scale(0.008, 0.05, 0);
    gl.uniformMatrix4fv(uTransform, false, transform.elements);
    gl.drawArrays(gl.TRIANGLE_STRIP, 0, 4);

    //Seconds
    transform.setTranslate(0, 0.55, 0);
    transform.rotate(-(360/60*time.getSeconds())%360, 0, 0, 1);
    transform.translate(0, 0.03, 0);
    transform.scale(0.004, 0.055, 0);
    gl.uniformMatrix4fv(uTransform, false, transform.elements);
    gl.drawArrays(gl.TRIANGLE_STRIP, 4, 4);
}

function tick() {
    gl.clear(gl.COLOR_BUFFER_BIT);

    drawGrass();
    drawBuildings();

    drawBase();
    drawBody();
    drawFace();
    drawRoof();

    drawClock();
    drawHands();

    window.requestAnimationFrame(tick);
}

tick();
