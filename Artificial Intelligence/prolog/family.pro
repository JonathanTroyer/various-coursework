male(robert).
male(phillip).
male(bryan).
male(tim).
male(daniel).
female(kim).
female(reba).
parent(robert,tim).
parent(robert,kim).
parent(phillip,tim).
parent(phillip,kim).
parent(bryan,tim).
parent(bryan,kim).
parent(daniel,phillip).
parent(reba,bryan).

mother(X,Y) :- parent(X,Y), female(Y).
father(X,Y) :- parent(X,Y), male(Y).
grandparent(X,Z) :- parent(X,Y), parent(Y,Z).

sibling(X,Y) :- parent(X,Z), parent(Y,Z), X \= Y.