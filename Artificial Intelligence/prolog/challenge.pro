% Jonathan Troyer
use_module(library(clpfd)).

even(X) :- 0 is X mod 2.

second([_,X|_],X).

gcd(A,0,A) :- !.
gcd(A,B,X) :- Q is A mod B, gcd(B, Q, X).

sum([X], X) :- !.
sum([H|T], X) :- sum(T, Part), X is H + Part.

partition([],_,[],[]).
partition([H|T],P,[H|A],B) :-
     H < P, !,
     partition(T,P,A,B).
partition([H|T],P,A,[H|B]) :-
    H >= P, !,
    partition(T,P,A,B).

quicksort([], []).
quicksort([A], [A]) :- !.
quicksort([H|T], X) :-
    partition(T, H, Lower, Upper),
    quicksort(Lower, LowerSorted),
    quicksort(Upper, UpperSorted),
    append(LowerSorted, [H], Tmp),
    append(Tmp, UpperSorted, X).