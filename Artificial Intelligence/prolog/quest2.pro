:- use_module(library(clpfd)).

concat([], B, B).
concat([H|T], B, [H|T2]) :- concat(T, B, T2).

doublelist(A,X) :- concat(A,A,X).

doubleelements([X], [X,X]) :- !.
doubleelements([H|T], X) :-
    DoubleHead = [H,H],
    doubleelements(T, DoubleTail),
    concat(DoubleHead,DoubleTail,X).

unzip([Odd], [Odd], []) :- !.
unzip([Odd,Even], [Odd], [Even]) :- !.
unzip([Odd,Even|Tail], B, C) :-
    concat([Odd], B2, B),
    concat([Even], C2, C),
    unzip(Tail, B2, C2).

% Creates a list of integers from first arg to second arg
range(A, A, B) :- !, B = [A].
range(A, C, [A|D]) :-
    A =< C,
    B is A+1,
    range(B, C, D).

none_divide([A], X) :- 0 #\= X mod A, !.
none_divide([H|T], X) :- 0 #\= X mod H, none_divide(T, X).

% Yeah I'm cheap. Deal with it 😎
prime(2) :- !.
prime(3) :- !.
prime(X) :-
    Bound is floor(sqrt(X)),
    range(2,Bound,Y),
    none_divide(Y, X).

% Doesn't solve the second case
% See https://stackoverflow.com/a/9069186 for something that does.
% I can't quite comprehend it yet.
flat([], []) :- !.
flat([H|T], X) :-
    !, % Added to ensure we don't keep building up lists forever
    flat(H, FlatH),
    flat(T, FlatT),
    concat(FlatH, FlatT, X).
flat(A, [A]). % Put after to make sure we try to flatten (above) before "rebuilding" an atom

% Creates a list of the atom paired with every element in the list
map(Atom, [Single], [Atom,Single]) :- !.
map(Atom, [Head|Tail], X) :-
    map(Atom, Tail, TailMap),
    concat([[Atom, Head]], [TailMap], X).

cross([], _, []).
cross([Head|Tail], List, X) :-
    map(Head, List, HeadMap),
    cross(Tail, List, TailCross),
    concat(HeadMap, TailCross, X).


% I know this is a monstrosity. Stop judging me, my brain is still in OO/imperative land

% Basically just a wrapper around trapsp_ that first checks if it's rectangular
transp(List, B) :-
    check_rec(List, _),
    transp_(List, B).

% Yeah I know I have a lot of helpers. But look how nicely it reads!!!
transp_([], []) :- !.
transp_(List, [BHead|BTail]) :-
    grab_tops(List, BHead),
    remove_tops(List, ToplessA),
    transp_(ToplessA, BTail).


% Checks if a list is rectangular, and gives the size if it is
check_rec([List], Length) :- !, list_length(List, Length).
check_rec([Head|Tail], Length) :-
    list_length(Head, Length),
    check_rec(Tail, Length).

list_length([], 0) :- !.
list_length([_], 1) :- !.
list_length([_|Tail], X) :- list_length(Tail, Y), X is Y + 1.

% Grabs the first elements of an array of arrays
grab_tops([Single], [Top]) :-
    !,
    first_ele(Single, Top).
grab_tops([Head|Tail], Tops) :-
    first_ele(Head, HeadTop),
    grab_tops(Tail, TailTops),
    concat([HeadTop], TailTops, Tops).

% Grabs the first element of an array
first_ele([Head|_], Head).

% Removes the first elements from an array of arrays
remove_tops([Single], X) :- !, remove_top(Single, X).
remove_tops([Head|Tail], Topless) :-
    remove_top(Head, ToplessHead),
    remove_tops(Tail, ToplessTail),
    concat(ToplessHead, ToplessTail, Topless).

% Removes the first element of an array
remove_top([_], []) :- !.
remove_top([_|Tail], [Tail]).
