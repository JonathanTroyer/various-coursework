:- use_module(library(clpfd)).

:- dynamic fib/2.

fib(0,1).
fib(1,1).
fib(N,M) :-
    A is N-1,
    B is N-2,
    fib(A,X),
    fib(B,Y),
    M is X+Y,
    asserta(fib(N,M)).

max([],_) :- fail.
max([X],X).
max([X|T],X) :- max(T,Y), X >= Y.
max([X|T],Y) :- max(T,Y), X < Y.

second_largest(List, Second) :-
    max_list(List, Q),
    select(Q, List, Rest),
    max_list(Rest, Second),
    !.

kth_largest(List, K, Lrg) :- K is 1, max_list(List, Lrg).
kth_largest(List, K, Lrg) :-
    K > 1,
    max_list(List, X),
    select(X, List, ListWOLargest),
    Q is K - 1,
    kth_largest(ListWOLargest, Q, Lrg).