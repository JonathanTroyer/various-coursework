:- use_module(library(clpfd)).

sum([X],X).
sum([A|T], X) :- sum(T,Y), X = A + Y.

value([X],X).
value([X|T],A) :- length(T,U), pow(10,U,V), value(T,B), A = X * V + B. 

notin(_,[]).
notin(X,[Y|A]) :- X \== Y, notin(X,A).

isin(X,[Y|_]) :- X == Y.
isin(X,[Y|B]) :- X \== Y, isin(X,B).

distinct(X,Y) :- distinct(X,[],Y).
distinct([],A,A).
distinct([X|Y],A,B) :- notin(X,A), distinct(Y,[X|A],B).
distinct([X|Y],A,B) :- isin(X,A), distinct(Y,A,B).

noleadingzero([X|_]) :- X #\= 0.

cryptarithm(A,C) :-  append(A,[C],U), append(U,V), 
					 distinct(V,W), W ins 0..9, 
					 all_different(W), maplist(noleadingzero,U), 
					 maplist(value,A,X), value(C,Y), 
					 sum(X,Z), Z #= Y, label(W).
