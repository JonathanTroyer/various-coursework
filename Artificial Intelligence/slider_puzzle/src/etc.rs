use types::*;

pub fn get_pos(
    pos: (usize, usize),
    dir: Direction,
    bounds: (usize, usize),
) -> Option<(usize, usize)> {
    // Note that this might seem backwards. This is because the assignment
    // asks for moves to be listed in terms of sliding a piece into the open
    // position, while we act like we're moving the open position around
    let new_pos = match dir {
        Direction::Up => Some((pos.0 as isize + 1, pos.1 as isize)),
        Direction::Down => Some((pos.0 as isize - 1, pos.1 as isize)),
        Direction::Left => Some((pos.0 as isize, pos.1 as isize + 1)),
        Direction::Right => Some((pos.0 as isize, pos.1 as isize - 1)),
    };

    match new_pos {
        Some((new_row, new_col)) => {
            if new_row < 0 || new_row >= bounds.0 as isize {
                None
            } else if new_col < 0 || new_col >= bounds.1 as isize {
                None
            } else {
                Some((new_row as usize, new_col as usize))
            }
        }
        None => None,
    }
}

pub fn generate_puzzle(puzzle: &Puzzle, dir: Direction, bounds: (usize, usize)) -> Option<Puzzle> {
    let open_pos = puzzle.open_pos;
    let new_open_pos = get_pos(open_pos, dir, bounds);

    match new_open_pos {
        Some(new_open_pos) => {
            let mut puzzle_cpy = puzzle.clone();
            move_piece(&mut puzzle_cpy, new_open_pos, open_pos);
            puzzle_cpy.open_pos = new_open_pos;
            Some(puzzle_cpy)
        }
        None => None,
    }
}

pub fn move_piece(puzzle: &mut Puzzle, a: (usize, usize), b: (usize, usize)) {
    puzzle.positions[puzzle.board[a.0][a.1]] = b;
    puzzle.positions[puzzle.board[b.0][b.1]] = a;
    let tmp = puzzle.board[a.0][a.1];
    puzzle.board[a.0][a.1] = puzzle.board[b.0][b.1];
    puzzle.board[b.0][b.1] = tmp;
}

pub fn calc_pdb_index(
    board_positions: &Vec<(usize, usize)>,
    pieces: &Vec<usize>,
    m: usize,
    n: usize,
) -> usize {
    let mut positions = Vec::new();
    for piece in pieces {
        let pos = board_positions[*piece];
        positions.push(pos.0 * n + pos.1);
    }

    let length = positions.len();
    for idx in 0..(length - 1) {
        for i in idx + 1..length {
            if positions[i] > positions[idx] {
                positions[i] -= 1;
            } else if positions[i] == positions[idx] {
                panic!("Two pieces in the same position when getting PDB index");
            }
        }
    }

    let mut count = 0;
    let mut pow = m * n;
    for pos in positions {
        count = count * pow + pos;
        pow -= 1;
    }

    return count;
}

pub fn print_puzzle(puzzle: &Puzzle) {
    for row in &puzzle.board {
        for col in row {
            print!("{} ", col);
        }
        println!("");
    }
}

pub fn make_puzzle(board: &Vec<Vec<usize>>) -> Puzzle {
    let mut positions = Vec::new();
    positions.resize(board.len() * board[0].len(), (0, 0));
    let mut row = 0;
    let mut open_pos = (0, 0);
    for line in board {
        let mut col = 0;
        for piece in line {
            positions[*piece] = (row, col);
            if *piece == 0 {
                open_pos = (row, col);
            }
            col += 1;
        }
        row += 1;
    }
    Puzzle {
        board: board.clone(),
        positions: positions,
        open_pos: open_pos,
    }
}
