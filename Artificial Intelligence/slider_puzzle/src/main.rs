use etc::*;
use std::collections::BinaryHeap;
use std::fmt::Write;
use std::io;
use std::io::prelude::*;
use std::mem::size_of;
use std::rc::Rc;
use types::*;

mod etc;
mod types;

const MOVE_DIRS: [Direction; 4] = [
    Direction::Left,
    Direction::Right,
    Direction::Up,
    Direction::Down,
];

fn main() {
    let mut input = Vec::new();
    let stdin = io::stdin();
    let mut n = None;
    for line in stdin.lock().lines() {
        let line = line.unwrap();
        let mut nums = Vec::new();
        for n in line.split(" ") {
            let item = n.trim().parse::<usize>();
            match item {
                Ok(n) => nums.push(n),
                Err(_) => nums.push(0),
            }
        }
        if let Some(n) = n {
            assert_eq!(n, nums.len());
        } else {
            n = Some(nums.len());
        }

        input.push(nums);
    }

    println!("\n------------\n");

    let length = input.len();
    assert_eq!(length % 2, 0);

    let m = length / 2;
    let n = n.unwrap();

    let target = input.split_off(m);
    let start = input;

    let start = make_puzzle(&start);
    let target = make_puzzle(&target);

    println!("Starting layout:");
    print_puzzle(&start);

    println!("Target layout:");
    print_puzzle(&target);

    println!("\n------------\n");

    // Construct the pattern DB
    println!("Generating pattern db");
    let pattern_db = generate_pdb(m, n, &target);
    let length = pattern_db.len();
    let mut size = size_of::<PatternDb>() * length;
    for db in &pattern_db {
        size += size_of::<char>() * db.tiles.len();
        size += size_of::<u8>() * db.entries.len();
    }
    println!("Pattern db is {} bytes", size);

    println!("Searching...");
    let mut open: BinaryHeap<PuzzleState> = BinaryHeap::new();
    open.push(PuzzleState {
        f_val: 0,
        puzzle: start,
        parent: None,
        move_from_parent: None,
        total_moves: 0,
    });

    let mut closed: Vec<PuzzleState> = Vec::new();

    let mut nodes_expanded = 0;
    let mut final_state = None;
    let start = std::time::Instant::now();
    while let Some(next) = open.pop() {
        let mut found = false;
        for dir in MOVE_DIRS.iter() {
            match generate_puzzle(&next.puzzle, *dir, (m, n)) {
                Some(p) => {
                    nodes_expanded += 1;
                    let heuristic: usize = pattern_db
                        .iter()
                        .map(|pdb| {
                            pdb.entries[calc_pdb_index(&p.positions, &pdb.tiles, m, n)] as usize
                        }).sum();

                    let total_moves = next.total_moves + 1;
                    let child = PuzzleState {
                        f_val: total_moves + heuristic,
                        puzzle: p,
                        parent: Some(Rc::new(next.clone())),
                        move_from_parent: Some(*dir),
                        total_moves: total_moves,
                    };
                    if child.puzzle == target {
                        final_state = Some(child);
                        found = true;
                        break;
                    }

                    let mut should_add = true;
                    for node in &open {
                        if child.puzzle == node.puzzle && child.f_val > node.f_val {
                            should_add = false;
                            break;
                        }
                    }
                    for node in &closed {
                        if child.puzzle == node.puzzle && child.f_val > node.f_val {
                            should_add = false;
                            break;
                        }
                    }

                    if should_add {
                        open.push(child);
                    }
                }
                None => (),
            }
        }
        if found {
            break;
        }

        closed.push(next);
    }
    let elapsed = start.elapsed();

    if let Some(final_state) = final_state {
        println!("Found answer: ");
        let mut moves = String::new();
        let mut puzzle_state = Some(Rc::new(final_state));
        while let Some(current) = puzzle_state {
            match current.move_from_parent {
                Some(parent_move) => {
                    write!(&mut moves, "{}\n", parent_move);
                }
                None => (),
            };
            puzzle_state = current.parent.clone();
        }
        println!("{}", moves);
        println!(
            "Expanded {} nodes in {} milliseconds",
            nodes_expanded,
            (elapsed.as_secs() * 1000) + elapsed.subsec_millis() as u64
        );
    }
}

fn generate_pdb(m: usize, n: usize, target: &Puzzle) -> Vec<PatternDb> {
    let mut pattern_dbs = Vec::new();
    for row in &target.board {
        let mut size = m * n;
        let mut total_perms = 1;
        for _ in 0..row.len() {
            total_perms *= size;
            size -= 1;
        }
        let mut entries = Vec::new();
        entries.resize(total_perms, u8::max_value());

        let mut next = Vec::new();
        next.push(target.clone());
        entries[calc_pdb_index(&target.positions, row, m, n)] = 0;

        let mut current_cost = 1;
        while next.len() > 0 {
            let prev = next;
            next = Vec::new();
            for puzzle in prev {
                for piece in row {
                    let piece_pos = puzzle.positions[*piece];
                    for dir in MOVE_DIRS.iter() {
                        match get_pos(piece_pos, *dir, (m, n)) {
                            Some(pos) => {
                                let mut puzzle_cpy = puzzle.clone();
                                if !row.contains(&puzzle_cpy.board[pos.0][pos.1]) {
                                    move_piece(&mut puzzle_cpy, pos, piece_pos);
                                    let idx = calc_pdb_index(&puzzle_cpy.positions, row, m, n);
                                    if entries[idx] == 255 {
                                        entries[idx] = current_cost;
                                        next.push(puzzle_cpy);
                                    }
                                }
                            }
                            None => (),
                        }
                    }
                }
            }
            current_cost += 1;
        }
        pattern_dbs.push(PatternDb {
            tiles: row.clone(),
            entries: entries,
        })
    }

    return pattern_dbs;
}
