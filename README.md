# Various Coursework

Various coursework created during my time at Oklahoma Christian University

These projects are given as-is. In many cases (especially the web projects), they may no longer work, or will need some configuration changes to get working. Feel free to open an issue if you want to get something working again.
