﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryManager : MonoBehaviour, IGameManager {

	public ManagerStatus status { get; private set; }

	public ItemType equippedItem{ get; private set; }

	private Dictionary<ItemType, int> items = new Dictionary<ItemType, int>();

	public void Startup() {
		Debug.Log("Starting inventory manager");

		equippedItem = ItemType.None;
		status = ManagerStatus.Started;
	}

	/*
	private void DisplayItems() {
		string itemDisplay = "Items: ";
		foreach (KeyValuePair<ItemType, int> item in items)
			itemDisplay += item.Key + "(" + item.Value + ") ";
		
		Debug.Log(itemDisplay);
	}
	*/
	public void AddItem(ItemType item) {
		if (items.ContainsKey(item))
			items[item]++;
		else
			items[item] = 1;

		//DisplayItems();
	}

	public IEnumerable GetItems() {
		return items.Keys;
	}

	public int GetItemCount(ItemType item) {
		if (items.ContainsKey(item))
			return items[item];
		
		return 0;
	}

	public bool EquipItem(ItemType item){
		if (items.ContainsKey(item) && equippedItem != item) {
			equippedItem = item;
			Debug.Log("Equipped " + item);
			return true;
		}
		equippedItem = ItemType.None;
		Debug.Log("Unequipped");
		return false;
	}

	public bool Consume(ItemType item){
		if (items.ContainsKey(item)) {
			items[item]--;
			if (items[item] == 0)
				items.Remove(item);
		} else
			return false;

		//DisplayItems();
		return true;
	}
}
