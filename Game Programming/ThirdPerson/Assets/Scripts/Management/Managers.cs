﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(InventoryManager))]
[RequireComponent(typeof(PlayerManager))]
public class Managers : MonoBehaviour {

	public static PlayerManager Player { get; private set; }
	public static InventoryManager Inventory { get; private set; }

	private List<IGameManager> startSequence = new List<IGameManager>();

	void Awake() {
		Player = GetComponent<PlayerManager>();
		Inventory = GetComponent<InventoryManager>();

		startSequence.Add(Player);
		startSequence.Add(Inventory);

		StartCoroutine(StartupManagers());
	}

	private IEnumerator StartupManagers() {
		foreach (IGameManager manager in startSequence) {
			manager.Startup();
		}

		yield return null;

		int modulesCount = startSequence.Count;
		int modulesReady = 0;

		while (modulesReady < modulesCount) {
			modulesReady = 0;

			foreach (IGameManager manager in startSequence) {
				if (manager.status == ManagerStatus.Started) {
					modulesReady++;
				}
			}

			yield return null;
		}

		Debug.Log("All managers started!");
	}
}
