﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeviceOperator : MonoBehaviour {

    public float operateDistance = 1.5f;
	
	void Update () {
        if (Input.GetButtonDown("Fire3")) {
            Collider[] hitColliders = Physics.OverlapSphere(transform.position, operateDistance);
            foreach (Collider hitCollider in hitColliders) {
                Vector3 dir = hitCollider.transform.position - transform.position;
                if (Vector3.Dot(transform.forward, dir) > 0.5f) {
                    hitCollider.SendMessage("Operate", SendMessageOptions.DontRequireReceiver);
                }
            }
        }
	}
}
