﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallSpawner : MonoBehaviour {

    public int numToSpawn;
    public float spawnArea = 29.0f;
    public float spawnAreaHeight = 100.0f;

    void Start() {
        StartCoroutine(spawn());
    }

    private IEnumerator spawn() {
        yield return new WaitForSeconds(5);

        for (int i = 1; i <= numToSpawn; i++) {
            var sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            sphere.transform.position = new Vector3(Random.Range(-spawnArea, spawnArea), 
                                                    Random.Range(1.0f, spawnAreaHeight), 
                                                    Random.Range(-spawnArea, spawnArea));
            var body = sphere.AddComponent<Rigidbody>() as Rigidbody;
            body.mass = 0.1f;
        }
    }
}
