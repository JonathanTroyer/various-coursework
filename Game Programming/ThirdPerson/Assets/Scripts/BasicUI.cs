﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class BasicUI : MonoBehaviour {

	void OnGUI() {
		int posX = 10;
		int posY = 10;
		int width = 100;
		int height = 30;
		int padding = 10;

		//Make immutable to avoid out of sync issues
		var items = Managers.Inventory.GetItems().Cast<ItemType>().ToList();
		bool hasItems = false;
		foreach (ItemType item in items) {
			//Draw items
			hasItems = true;
			int count = Managers.Inventory.GetItemCount(item);
			Texture2D icon = Resources.Load<Texture2D>("Icons/" + item);
			GUI.Box(new Rect(posX, posY, width, height), new GUIContent(count.ToString(), icon));

			//Draw equip buttoms
			if (GUI.Button(new Rect(posX, posY + height + padding, width, height), "Equip " + item))
				Managers.Inventory.EquipItem(item);

			if (item == ItemType.Health) {
				if (GUI.Button(new Rect(posX, posY + height * 2 + padding * 2, width, height), "Use")) {
					Managers.Inventory.Consume(item);
					Managers.Player.ChangeHealth(25);
				}
			}

			posX += width + padding;
		}

		if (!hasItems)
			GUI.Box(new Rect(posX, posY, width, height), "No items");

		ItemType equipped = Managers.Inventory.equippedItem;
		if (equipped != ItemType.None) {
			posX = Screen.width - (width + padding);
			Texture2D icon = Resources.Load<Texture2D>("Icons/" + equipped);
			GUI.Box(new Rect(posX, posY, width, height), new GUIContent("Equipped", icon));
		}

		posX = 10;
		posY += height + padding;
		foreach (ItemType item in items) {
			
			posX += width + padding;
		}
	}
}
