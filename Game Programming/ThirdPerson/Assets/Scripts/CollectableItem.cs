﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectableItem : MonoBehaviour {

	[SerializeField] private ItemType item;

	void OnTriggerEnter(Collider other) {
		Managers.Inventory.AddItem(item);
		Destroy(this.gameObject);
	}
}

public enum ItemType {
	Health,
	Energy,
	Key,
	Ore,
	None
}
