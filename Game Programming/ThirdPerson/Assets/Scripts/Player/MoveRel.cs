﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterController))]
[RequireComponent(typeof(Animator))]
public class MoveRel : MonoBehaviour {

	[SerializeField] Transform screenTarget;

	public float moveSpeed = 6.0f;
	public float rotSpeed = 15.0f;
	public float jumpSpeed = 15.0f;
	public float mass = 3.0f;

	public float gravity = -9.8f;
	public float terminalVelocity = 50.0f;
	public float minFall = -1.5f;

	private Animator animator;
	private CharacterController charController;
	private ControllerColliderHit contact;
	private float vertSpeed;

	void Start() {
		charController = GetComponent<CharacterController>();
		animator = GetComponent<Animator>();
	}

	void Update() {
		Vector3 movement = Vector3.zero;

		//Get ground movement
		float inputHorz = Input.GetAxis("Horizontal");
		float inputVert = Input.GetAxis("Vertical");
		if (inputHorz != 0 || inputVert != 0) {
			movement.x = inputHorz * moveSpeed;
			movement.z = inputVert * moveSpeed;
			movement = Vector3.ClampMagnitude(movement, moveSpeed);

			var tempRot = screenTarget.rotation;
			screenTarget.eulerAngles = new Vector3(0, screenTarget.eulerAngles.y, 0);
			movement = screenTarget.TransformDirection(movement);
			screenTarget.rotation = tempRot;

			var dir = Quaternion.LookRotation(movement);
			transform.rotation = Quaternion.Slerp(transform.rotation, dir, rotSpeed * Time.deltaTime);
		}

		animator.SetFloat("speed", movement.sqrMagnitude);

		//Get verticalMovement
		//Handle 'lip' case
		bool hitGround = false;
		RaycastHit hit;
		if (vertSpeed <= 0 && Physics.Raycast(transform.position, Vector3.down, out hit)) {
			float check = (charController.height + charController.radius) / 1.9f;
			hitGround = hit.distance <= check;
		}

		if (hitGround) {
			if (Input.GetButtonDown("Jump")) {
				vertSpeed = jumpSpeed;
			} else {
				vertSpeed = minFall;
				animator.SetBool("jumping", false);
			}
		} else {
			vertSpeed += gravity * Time.deltaTime;
			//if (contact != null) {
			animator.SetBool("jumping", true);
			//}
			//Check 'slide' case
			if (charController.isGrounded) {
				if (Vector3.Dot(movement, contact.normal) < 0) {
					movement = contact.normal * moveSpeed;
				} else {
					movement += contact.normal * moveSpeed;
				}
			}
		}
		movement.y = Mathf.Clamp(vertSpeed, -terminalVelocity, terminalVelocity);

		//Move
		movement *= Time.deltaTime;
		charController.Move(movement);
	}

	void OnControllerColliderHit(ControllerColliderHit hit) {
		contact = hit;

		Rigidbody body = hit.collider.attachedRigidbody;
		if (body != null && !body.isKinematic) {
			body.velocity = hit.moveDirection * mass;
		}
	}
}
