﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorOpen : MonoBehaviour {

    [SerializeField] private Vector3 offset;
    public float operateTime = 1.0f;

    private Vector3 closedPosition;
    private Vector3 openPosition;
	private Vector3 fromPosition;
    private bool isOpen = false;
    private float time = 0.0f;

    void Start() {
        closedPosition = transform.position;
        openPosition = closedPosition + offset;
    }

    void Update() {
        if (!isOpen && transform.position != closedPosition) {
            time += Time.deltaTime;
            transform.position = Vector3.Lerp(fromPosition, closedPosition, time / operateTime);
        }
        else if (isOpen && transform.position != openPosition) {
            time += Time.deltaTime;
			transform.position = Vector3.Lerp(fromPosition, openPosition, time / operateTime);
        }
    }

    public void Operate() {
        isOpen = !isOpen;
		startOpen();
    }

	public void Activate(){
		isOpen = true;
		startOpen();
	}

	public void Deactivate(){
		isOpen = false;
		startOpen();
	}

	private void startOpen(){
		time = 0.0f;
		fromPosition = transform.position;
	}
}
