﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Renderer))]
public class ColorChanger : MonoBehaviour {

	public void Operate() {
		GetComponent<Renderer>().material.color = 
			new Color (
				Random.Range(0.0f, 1.0f), 
				Random.Range(0.0f, 1.0f),
				Random.Range(0.0f, 1.0f)
			);
	}
}
