﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraOrbit : MonoBehaviour {

    [SerializeField] private Transform target;
    public float keySpeed = 1.0f;
    public float mouseSpeed = 3.0f;
    public bool enableKeyboardControl = false;

    private float rotY;
    private Vector3 offset;

    void Start() {
        rotY = transform.eulerAngles.y;
        offset = target.position - transform.position;

        Cursor.lockState = CursorLockMode.Locked;
        HideCursor(true);
    }

    void LateUpdate() {
        
        float inputHorz = Input.GetAxis("Horizontal");
        if (enableKeyboardControl && inputHorz != 0) {
            rotY += inputHorz * keySpeed;
        }
        else {

            rotY += Input.GetAxis("Mouse X") * mouseSpeed * ((Cursor.visible) ? 0 : 1);
        }

        var rotation = Quaternion.Euler(0, rotY, 0);
        transform.position = target.position - (rotation * offset);
        transform.LookAt(target);

        if (Input.GetKeyDown(KeyCode.Escape)) {
            ToggleCursor();
        }
    }

    public static void HideCursor(bool locked) {
        Cursor.visible = !locked;
    }

    public static void ToggleCursor() {
        Cursor.visible = !Cursor.visible;
    }
}
