﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof (CharacterController))]
[RequireComponent(typeof (Animator))]
public class MoveRel : MonoBehaviour {

    [SerializeField] Transform screenTarget;

    public float moveSpeed = 6.0f;
    public float rotSpeed = 15.0f;
    public float mass = 3.0f;

    private Animator animator;
    private CharacterController charController;

    void Start() {
        charController = GetComponent<CharacterController>();
        animator = GetComponent<Animator>();
    }

    void Update() {
        Vector3 movement = Vector3.zero;

        //Get ground movement
        float inputHorz = Input.GetAxis("Horizontal");
        float inputVert = Input.GetAxis("Vertical");
        if (inputHorz != 0 || inputVert != 0) {
            movement.x = inputHorz * moveSpeed;
            movement.z = inputVert * moveSpeed;
            movement = Vector3.ClampMagnitude(movement, moveSpeed);

            var tempRot = screenTarget.rotation;
            screenTarget.eulerAngles = new Vector3(0, screenTarget.eulerAngles.y, 0);
            movement = screenTarget.TransformDirection(movement);
            screenTarget.rotation = tempRot;

            var dir = Quaternion.LookRotation(movement);
            transform.rotation = Quaternion.Slerp(transform.rotation, dir, rotSpeed * Time.deltaTime);
        }

        animator.SetFloat("speed", movement.sqrMagnitude);

        RaycastHit grounded;
        if (!Physics.Raycast(transform.position, Vector3.down, 2.0f)) {
            animator.SetBool("dead", true);
        }

        //Move
        movement *= Time.deltaTime;
        charController.Move(movement);

    }
}
