﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wander : MonoBehaviour {

    public float speed = 3.0f;
    public float range = 5.0f;

    private bool isAlive = true;

    void Update () {
        if (isAlive) {
            this.transform.Translate(0, 0, speed * Time.deltaTime);
        }

        Ray ray = new Ray(transform.position, transform.forward);
        RaycastHit hit;
        if (Physics.SphereCast(ray, 0.75f, out hit)) {     
            if (hit.distance < range) {
                this.transform.Rotate(0, Random.Range(-100, 100), 0);
            }
        }
    }

	public void Hit () {
		Wander ai = GetComponent<Wander>();
		isAlive = false;
		StartCoroutine(Die());
	}

	private IEnumerator Die () {
		this.transform.Rotate(-75, 0, 0);
		yield return new WaitForSeconds(1.5f);

		Destroy(this.gameObject);
	}
}
