﻿using System.Collections;
using UnityEngine;

[RequireComponent(typeof (CharacterController))]
public class Spin : MonoBehaviour {

	public float rate = 180;
	public float speed = 5;

	private CharacterController controller;

	void Start(){
		controller = GetComponent<CharacterController> ();
	}

	// Update is called once per frame
	void Update () {
		transform.Rotate (0, Time.deltaTime * rate, 0);
		var direction = new Vector3 (Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
		direction *= Time.deltaTime * speed;
		controller.Move (direction);
	}
}
