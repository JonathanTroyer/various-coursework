﻿using UnityEngine;

public class GunElevate : MonoBehaviour {

	public float rotationSpeed = 1;

	private float currentRotation = 0;

	// Update is called once per frame
	void Update () {
		currentRotation += Input.GetAxis ("Vertical") * rotationSpeed * Time.deltaTime;
		currentRotation = Mathf.Clamp (currentRotation, -45, 45);
		transform.localEulerAngles = new Vector3 (0, 0, currentRotation);
	}
}
