﻿using UnityEngine;

public class Move : MonoBehaviour {

	public float speed = 1;
	
	// Update is called once per frame
	void Update () {
		var input = Input.GetAxisRaw ("Horizontal");
		transform.Translate (input * Time.deltaTime * speed, 0, 0);
	}
}
