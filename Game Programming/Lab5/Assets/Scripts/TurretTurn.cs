﻿using UnityEngine;

public class TurretTurn : MonoBehaviour {

	public float rotationSpeed = 1;

	private float currentRotation = 90;
	
	// Update is called once per frame
	void Update () {
		currentRotation += Input.GetAxis ("Horizontal") * rotationSpeed * Time.deltaTime;
		currentRotation = Mathf.Clamp (currentRotation, 0, 180);
		transform.localEulerAngles = new Vector3 (0, currentRotation, 0);
	}
}
