﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class NavAgent : MonoBehaviour {

	[SerializeField] GameObject target;

	private NavMeshAgent agent;

	void Start () {
		agent = GetComponent<NavMeshAgent>();	
	}
	
	void Update () {
		this.agent.destination = this.target.transform.position;
	}
}
