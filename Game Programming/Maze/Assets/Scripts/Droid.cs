﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Droid : MonoBehaviour {

	public Node startNode;

	private Stack<Node> path;
	private bool canWalk = false;
	private float lerpAmount = 0.0f;
	private Vector3 start, end;

	private SceneController controller;

	public IEnumerator breadthFirstSearch(SceneController controller) {
		this.controller = controller;

		var openNodes = new List<Node>();
		var closedNodes = new List<Node>();
		openNodes.Add(startNode);

		bool foundPath = false;
		while (openNodes.Count > 0) {
			yield return null;
			var currentNode = openNodes[0];
			currentNode.waypoint.GetComponent<MeshRenderer>().material.color = Color.red;
			openNodes.RemoveAt(0);
			if (currentNode.waypoint.tag == "Goal") {
				foundPath = true;
				StartCoroutine(walkPath(currentNode));
				break;
			} else {
				var children = getChildren(currentNode);
				currentNode.children = children;
				closedNodes.Add(currentNode);
				foreach(Node child in children) {
					if (!(openNodes.Contains(child) || closedNodes.Contains(child))) {
						child.waypoint.GetComponent<MeshRenderer>().material.color = Color.blue;
						openNodes.Add(child);
					}
				}
			}
		}

		if (!foundPath)
			this.controller.setState(CreationState.Failed);
	}

	private IEnumerator walkPath(Node goal) {
		path = new Stack<Node>();
		goal.waypoint.GetComponent<MeshRenderer>().material.color = Color.green;
		path.Push(goal);
		while (path.Peek().parent != null) {
			yield return null;
			var pathNode = path.Peek().parent;
			pathNode.waypoint.GetComponent<MeshRenderer>().material.color = Color.green;
			path.Push(pathNode);
		}
		start = this.transform.position;
		end = path.Pop().waypoint.transform.position;
		canWalk = true;
		controller.setState(CreationState.Complete);
	}

	void Update() {
		if (canWalk) {
			if (this.transform.position == end) {
				if (path.Count > 0) {
					end = path.Pop().waypoint.transform.position;
					start = this.transform.position;
					lerpAmount = 0.0f;
				} else
					canWalk = false;
			}
			lerpAmount += Time.deltaTime / Vector3.Distance(start, end) * 100;
			this.transform.position = Vector3.Lerp(start, end, lerpAmount);
		}
	}

	private List<Node> getChildren(Node n) {
		RaycastHit northHit;
		bool hitNorth = Physics.Raycast(n.waypoint.transform.position, Vector3.forward, out northHit);
		RaycastHit southHit;
		bool hitSouth = Physics.Raycast(n.waypoint.transform.position, Vector3.back, out southHit);
		RaycastHit eastHit;
		bool hitEast = Physics.Raycast(n.waypoint.transform.position, Vector3.right, out eastHit);
		RaycastHit westHit;
		bool hitWest = Physics.Raycast(n.waypoint.transform.position, Vector3.left, out westHit);

		var nodes = new List<Node>();
		if (hitNorth && northHit.transform.gameObject.name.Contains("Waypoint"))
			nodes.Add(new Node(northHit.transform.gameObject, n));
		if (hitSouth && southHit.transform.gameObject.name.Contains("Waypoint"))
			nodes.Add(new Node(southHit.transform.gameObject, n));
		if (hitEast && eastHit.transform.gameObject.name.Contains("Waypoint"))
			nodes.Add(new Node(eastHit.transform.gameObject, n));
		if (hitWest && westHit.transform.gameObject.name.Contains("Waypoint"))
			nodes.Add(new Node(westHit.transform.gameObject, n));

		
		return nodes;
	}
}
