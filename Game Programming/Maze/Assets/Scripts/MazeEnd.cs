﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//Maze end is in charge of controlling the timer and is attatched to a box collider at the end of the maze
public class MazeEnd : MonoBehaviour {

	[SerializeField] Text text;

	private bool finished = true;
	private float timer = 0.0f;

	//When the player exits the collider, they've exited the maze
	void OnTriggerExit(Collider other) {
		finished = true;
	}

	//Only update the timer when the maze is incompllete
	void Update() {
		if (!finished) {
			timer += Time.deltaTime;
			text.text = string.Format("{0:#.0}", timer);
		}
	}

	public void startTimer() {
		finished = false;
	}

	public void setText(Text text) {
		this.text = text;
	}
}
