﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;

public class GridGenerator : MonoBehaviour {

	public int gridSize;

	public Vector3 entrancePosition{ get; private set; }

	[SerializeField] SceneController controller;
	[SerializeField] GameObject wallType, postType, floorType, triggerType;
	[SerializeField] bool showWaypoints = false;

	private float wallLength, wallHeight, wallWidth;
	private GameObject myFloor;
	private bool columnChunkFinished;
	private LinkedList<bool> mazeGenerators, columnGenerators;

	private GameObject gridContainer;
	private Cell[,] grid;
	private bool[,] visited;
	private GameObject waypointContainer;
	private int waypointCounter = 0;

	public void create() {
		setWallSize();
		createFloor();
		setCamera();
		this.recreate();
	}

	public void recreate() {
		controller.setState(CreationState.CreatingMaze);
		StartCoroutine(createGrid());
	}

	//Set the size of the walls for later calculations
	private void setWallSize() {
		var scale = wallType.transform.lossyScale;
		wallLength = scale.x;
		wallHeight = scale.y;
		wallWidth = scale.z;
	}

	//Move the camera to the center of the maze such that the whole maze can be seen
	private void setCamera() {
		var pos = myFloor.transform.position;
		pos.y = (gridSize * (wallLength + wallWidth) + 2 * wallHeight) / 2 * Mathf.Sqrt(3.0f);
		var c = controller.getCamera();
		c.transform.position = pos;
		c.farClipPlane = pos.y * 2;
	}

	//Spawn the flor to be a bit bigger than the maze itself
	private void createFloor() {
		var size = gridSize * (wallLength + wallWidth);
		Vector3 pos = new Vector3(size / 2.0f, 0.0f, size / 2.0f);
		myFloor = Instantiate(floorType, pos, Quaternion.identity);
		myFloor.name = "Floor";
		myFloor.transform.localScale = new Vector3(size / 10.0f + 1.0f, 1.0f, size / 10.0f + 1.0f);
		var rend = myFloor.GetComponent<Renderer>();
		//Rescale the texture so that we actually get tiling
		rend.material.mainTextureScale = new Vector2(gridSize, gridSize);
	}

	//Setup the array for the maze
	private IEnumerator createGrid() {
		if (gridContainer != null)
			destroyChildren(gridContainer);
		else {
			gridContainer = new GameObject();
			gridContainer.name = "Grid container";
			gridContainer.transform.parent = this.transform;
		}

		int chunkSize = 100;
		grid = new Cell[gridSize + 1, gridSize + 1];
		for (int i = 0; i <= gridSize; i += chunkSize) {
			columnChunkFinished = false;
			for (int j = i; j < i + chunkSize && j <= gridSize; j++) {
				StartCoroutine(createColumn(j));
			}
			while (!columnChunkFinished)
				yield return null;
		}
		createMaze();
	}

	private IEnumerator createColumn(int i) {
		for(int j = 0; j <= gridSize; j++) {
			yield return null;
			if (i == 0 && j != 0) {
				grid[i, j].eastWall = createWall(i, j, true);
			} else if (j == 0 && i != 0) {
				grid[i, j].northWall = createWall(i, j, false);
			} else if (!(i == 0 || j == 0)) {
				grid[i, j].northWall = createWall(i, j, false);
				grid[i, j].eastWall = createWall(i, j, true);
			}
			createPost(i, j);
		}

		//While technically all of them will finish at different times, it's close enough to be negligible so this is an easy way to detect if the columns are done
		columnChunkFinished = true;
	}

	private void createMaze() {
		if (waypointContainer != null)
			destroyChildren(waypointContainer);
		else {
			waypointContainer = new GameObject();
			waypointContainer.name = "Waypoint container";
			waypointContainer.transform.parent = this.transform.parent;
		}
			
		var rand = new System.Random();

		visited = new bool[gridSize + 1, gridSize + 1];

		int chunkSize = (gridSize / 10.0f > 1.0f) ? gridSize / 10 : 1;
		mazeGenerators = new LinkedList<bool>();

		for(int i = 1; i <= gridSize; i += 10) {
			for(int j = 1; j <= gridSize; j += 10) {
				mazeGenerators.AddLast(false);
				StartCoroutine(generateSection(rand, new Position(i, j)));
			}
		}
	}

	private IEnumerator generateSection(System.Random rand, Position startPos) {
		var positions = new Stack<Position>();
		positions.Push(startPos);

		while (positions.Count > 0) {
			var currentPos = positions.Peek();
			Debug.DrawRay(getCenterPos(currentPos.x, currentPos.y), Vector3.up * 100);
			visited[currentPos.x, currentPos.y] = true;
			var neighbors = getNeighbors(currentPos);
			if (neighbors.Length > 0) {
				var randNeigbor = neighbors[rand.Next(0, neighbors.Length)];
				GameObject toDestroy = null;
				if (randNeigbor.x > currentPos.x)
					toDestroy = grid[currentPos.x, currentPos.y].eastWall;
				else if (randNeigbor.x < currentPos.x)
					toDestroy = grid[randNeigbor.x, randNeigbor.y].eastWall;
				else if (randNeigbor.y > currentPos.y)
					toDestroy = grid[currentPos.x, currentPos.y].northWall;
				else if (randNeigbor.y < currentPos.y)
					toDestroy = grid[randNeigbor.x, randNeigbor.y].northWall;
				else
					throw new SystemException("The neighbor was the current position");

				toDestroy.tag = "Destroying";
				Destroy(toDestroy);

				if (!controller.isPlayerControl()) {
					var north = grid[currentPos.x, currentPos.y].northWall;
					var east = grid[currentPos.x, currentPos.y].eastWall;
					var south = grid[currentPos.x, currentPos.y - 1].northWall;
					var west = grid[currentPos.x - 1, currentPos.y].eastWall;

					bool noNorth = north == null || north.tag == "Destroying";
					bool noSouth = south == null || south.tag == "Destroying";
					bool noEast = east == null || east.tag == "Destroying";
					bool noWest = west == null || west.tag == "Destroying";

					if ((noNorth || noSouth) && (noEast || noWest))
						createWaypoint(currentPos);
				}

				positions.Push(randNeigbor);
			} else {
				var pos = positions.Pop();
				//If we've exhausted all our options, create an intersection
				if (positions.Count == 0) {
					//If we're not on the left edge, connect to the maze on the left
					if(pos.x > 1)
						Destroy(grid[pos.x - 1, pos.y].eastWall);
					//If we're not on the bottom edge, connect to the maze on the bottom
					if (pos.y > 1)
						Destroy(grid[pos.x, pos.y - 1].northWall);

					//Nuke the rest of the walls
					Destroy(grid[pos.x, pos.y].northWall);
					Destroy(grid[pos.x, pos.y].eastWall);

					//Create a waypoint at our guaranteed intersection
					createWaypoint(pos);
				}
			}

			yield return null;
		}
		mazeGenerators.RemoveFirst();
		mazeGenerators.AddLast(true);

		finishMaze(rand);
	}

	private void finishMaze(System.Random rand) {
		if (!mazeGenerators.Contains(false)) {
			var entrance = rand.Next(1, gridSize);
			entrancePosition = getCenterPos(0, entrance);
			if (!controller.isPlayerControl())
				controller.setStartNode(createWaypoint(new Position(1, entrance)));
			Destroy(grid[0, entrance].eastWall);

			var endPos = new Position(gridSize, rand.Next(1, gridSize));
			GameObject exit = grid[endPos.x, endPos.y].eastWall;
			var end = Instantiate(triggerType, exit.transform.position, exit.transform.rotation);
			if (!controller.isPlayerControl()) {
				createWaypoint(endPos);
				createWaypoint(new Position(endPos.x + 1, endPos.y)).tag = "Goal";
			}
			Destroy(exit);

			controller.setEnd(end.GetComponent<MazeEnd>());
			controller.setState(CreationState.CreatedMaze);
		}
	}

	private Position[] getNeighbors(Position pos) {
		var neighbors = new List<Position>();

		if (pos.x > 1 && !visited[pos.x - 1, pos.y]) {
			neighbors.Add(new Position(pos.x - 1, pos.y));
		}
		if (pos.x < gridSize && !visited[pos.x + 1, pos.y]) {
			neighbors.Add(new Position(pos.x + 1, pos.y));
		}
		if (pos.y > 1 && !visited[pos.x, pos.y - 1]) {
			neighbors.Add(new Position(pos.x, pos.y - 1));
		}
		if (pos.y < gridSize && !visited[pos.x, pos.y + 1]) {
			neighbors.Add(new Position(pos.x, pos.y + 1));
		}

		return neighbors.ToArray();
	}

	private GameObject createWall(int x, int y, bool isEast) {
		GameObject wall;
		if (isEast)
			wall = Instantiate(wallType, getEastWallPos(x, y), Quaternion.Euler(Vector3.up * 90), gridContainer.transform);
		else
			wall = Instantiate(wallType, getNorthWallPos(x, y), Quaternion.identity, gridContainer.transform);

		wall.name = String.Format("{2} wall {0}, {1}", x, y, isEast ? "East" : "North");
		return wall;
	}

	private GameObject createPost(int x, int y) {
		GameObject post = Instantiate(postType, getPostPos(x, y), Quaternion.identity, gridContainer.transform);
		post.name = String.Format("Post {0}, {1}", x, y);
		return post;
	}

	private GameObject createWaypoint(Position p) {
		GameObject waypoint = GameObject.CreatePrimitive(PrimitiveType.Cube);
		waypoint.name = "Waypoint " + waypointCounter;
		waypointCounter++;
		waypoint.transform.position = getCenterPos(p.x, p.y);
		waypoint.transform.parent = waypointContainer.transform;
		waypoint.transform.localScale = new Vector3(wallLength, wallHeight, wallLength);
		if (!showWaypoints)
			waypoint.GetComponent<MeshRenderer>().enabled = false;
		
		return waypoint;
	}

	private Vector3 getNorthWallPos(int x, int y) {
		return new Vector3(x * (wallLength + wallWidth) - (wallLength + wallWidth) / 2, wallHeight / 2.0f, y * (wallLength + wallWidth));
	}

	private Vector3 getEastWallPos(int x, int y) {
		return new Vector3(x * (wallLength + wallWidth), wallHeight / 2.0f, y * (wallLength + wallWidth) - (wallLength + wallWidth) / 2);
	}

	private Vector3 getPostPos(int x, int y) {
		return new Vector3(x * (wallLength + wallWidth), wallHeight / 2.0f, y * (wallLength + wallWidth));
	}

	private Vector3 getCenterPos(int x, int y) {
		var pos = getPostPos(x, y);
		var half = (wallLength + 2 * wallWidth) / 2;
		pos.x -= half;
		pos.z -= half;
		return pos;
	}

	private void destroyChildren(GameObject obj){
		var t = obj.transform;
		foreach(Transform child in t)
			Destroy(child.gameObject);
	}
}