﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Minimap is in charge of showing/hiding the overhead camera such that it functions as a minimap
public class Minimap : MonoBehaviour {

	private Camera mapCamera;

	public int logAmount = 5;
	public float waitSeconds = 0.1f;

	//Check for minimap toggle
	void Update() {
		if (Input.GetKeyDown(KeyCode.M)) {
			mapCamera.enabled = !mapCamera.enabled;
		}
	}

	//Setup the camera, put it in the corner, and put it on top
	public void setMapCamera(Camera camera) {
		this.mapCamera = camera;
		this.mapCamera.rect = new Rect(0.1f, 0.1f, 0.2f, 0.2f);
		this.mapCamera.depth = 9999;
		StartCoroutine(logCameraPosition());
	}

	//Temporary debuggin to see why the heck the camera is being moved
	private IEnumerator logCameraPosition() {
		Debug.Log(this.mapCamera.transform.position);
		logAmount--;
		yield return new WaitForSeconds(waitSeconds);
		if (logAmount > 0)
			StartCoroutine(logCameraPosition());
	}
}
