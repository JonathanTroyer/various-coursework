﻿//The currect state of creating the maze
public enum CreationState
{
	CreatingMaze,
	CreatedMaze,
	MovingCamera,
	Complete,
	Failed
}