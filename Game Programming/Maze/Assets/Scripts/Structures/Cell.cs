﻿using UnityEngine;

//Represents a single cell in a maze
struct Cell
{
	public GameObject northWall, eastWall;
}
