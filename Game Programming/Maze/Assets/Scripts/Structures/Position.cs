﻿using UnityEngine;

//A helper utility for indexing a 2D array easier
class Position {
	public int x, y;

	public Position(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public override string ToString() {
		return "X: " + x + " Y: " + y;
	}
}
