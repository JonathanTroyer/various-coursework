﻿using System.Collections.Generic;
using UnityEngine;
using System;

public class Node: IEquatable<Node> {

	public GameObject waypoint;

	public Node parent;
	public List<Node> children;

	public Node(GameObject waypoint, Node parent) {
		this.waypoint = waypoint;
		this.parent = parent;
	}

	public override bool Equals(System.Object obj) {
		if (obj == null || GetType() != obj.GetType())
			return false;

		return this.Equals((Node)obj);
	}

	public bool Equals(Node other) {
		return this.waypoint.name.Equals(other.waypoint.name);
	}

}
