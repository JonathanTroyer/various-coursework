﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SceneController : MonoBehaviour {

	[SerializeField] GridGenerator generator;
	[SerializeField] GameObject playerType;
	[SerializeField] Text timerText;
	[SerializeField] Camera mainCamera;
	public bool playerControl;

	private Node startNode;
	private GameObject droid;
	private CreationState creationState;
	private Camera tempCamera;
	private float lerpAmount;
	private MazeEnd end;

	void Start(){
		generator.create();
	}

	public CreationState getState() {
		return this.creationState;
	}

	//Set the state of creation, and trigger actions based on the state set
	public void setState(CreationState state) {
		this.creationState = state;
		Debug.Log(state);
		if (playerControl) {
			switch (state) {
				case CreationState.CreatedMaze:
					this.moveCamera();
					break;
				case CreationState.Complete: 
					this.allowControl();
					break;
			}
		} else {
			switch (state) {
				case CreationState.CreatedMaze:
					StartCoroutine(this.makeDroid());
					break;
				case CreationState.Failed:
					generator.recreate();
					break;
			}
		}
	}

	public bool isPlayerControl() {
		return playerControl;
	}

	public void setStartNode(GameObject node) {
		this.startNode = new Node(node, null);
	}

	//Store the MazeEnd so that we can set it's text (prefabs cannot have Inspector objects tied to them)
	public void setEnd(MazeEnd end) {
		this.end = end;
		this.end.setText(timerText);
	}

	public Camera getCamera() {
		return mainCamera;
	}

	//Create a temporary camera to lerp between the overhead and main
	private void moveCamera() {
		tempCamera = Instantiate(mainCamera);
		lerpAmount = 0.0f;
		this.setState(CreationState.MovingCamera);
	}

	//Create the player and minimap, and start the timer
	private void allowControl() {
		Destroy(tempCamera.gameObject);
		var player = Instantiate(playerType, generator.entrancePosition, Quaternion.identity);
		player.transform.LookAt(new Vector3(mainCamera.transform.position.x, player.transform.position.y, mainCamera.transform.position.z));
		var map = player.AddComponent<Minimap>();
		map.setMapCamera(this.mainCamera);
		this.end.startTimer();
	}

	private IEnumerator makeDroid() {
		Destroy(droid);

		droid = GameObject.CreatePrimitive(PrimitiveType.Capsule);
		droid.transform.localScale = new Vector3(1, 100, 1);
		Droid d = droid.AddComponent<Droid>();
		d.startNode = startNode;
		d.transform.position = startNode.waypoint.transform.position;
		yield return new WaitForSeconds(1.0f); //Basically a hack to let Unity finish instantiating stuff
		StartCoroutine(d.breadthFirstSearch(this));
	}

	//If we're moving the camera, lerp between overhead and player
	void Update() {
		if (this.creationState == CreationState.MovingCamera) {
			lerpAmount += Time.deltaTime / 2.0f;
			tempCamera.transform.position = Vector3.Lerp(mainCamera.transform.position, generator.entrancePosition, lerpAmount);
			//Look at the center of the maze (we know that the main camera is centered on it, so we use that)
			tempCamera.transform.LookAt(new Vector3(mainCamera.transform.position.x, 0, mainCamera.transform.position.z));
			if (tempCamera.transform.position == generator.entrancePosition)
				this.setState(CreationState.Complete);
		}
	}
}
