﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TargetController : MonoBehaviour {

    [SerializeField] private Text text;
    private float moveSpeed = 2.0f;
    private int hitCount = 0;

    void Update() {
        transform.position += transform.right * moveSpeed * Time.deltaTime;
    }

    void OnTriggerEnter() {
        moveSpeed = -moveSpeed;
    }

    void OnCollisionEnter() {
        hitCount++;
        text.text = "Hits: " + hitCount;
    }
}
