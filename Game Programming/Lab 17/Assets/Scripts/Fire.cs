﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fire : MonoBehaviour {

    public float fireForce = 5.0f;
    [SerializeField] private GameObject cannonType;

	void Update () {
        if (Input.GetKeyDown(KeyCode.Space)) {
            var cannonball = Instantiate(cannonType) as GameObject;
            cannonball.transform.position = this.transform.position + transform.up * 0.75f;
            var body = cannonball.AddComponent<Rigidbody>();
            body.AddForce(transform.up * fireForce);
        }
	}
}
