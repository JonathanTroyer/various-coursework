﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cannonball : MonoBehaviour {

	void Start () {
        StartCoroutine(Despawn());
	}
	
    private IEnumerator Despawn () {
        yield return new WaitForSeconds(3);
        Destroy(this.gameObject);
    }
}
