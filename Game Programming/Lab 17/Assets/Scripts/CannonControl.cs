﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CannonControl : MonoBehaviour {

    public float rotationSpeed = 15.0f;

    private float rotationHorizontal = 0.0f;
    private float rotationVertical = 0.0f;
	
	void Update () {
        rotationHorizontal += Input.GetAxis("Horizontal");
        rotationHorizontal = Mathf.Clamp(rotationHorizontal, -90.0f, 90.0f);
        rotationVertical -= Input.GetAxis("Vertical");
        rotationVertical = Mathf.Clamp(rotationVertical, -60.0f, 10.0f);

        transform.localEulerAngles = new Vector3(rotationVertical, rotationHorizontal, 0) * rotationSpeed * Time.deltaTime;
	}
}
