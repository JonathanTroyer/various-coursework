﻿using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class UIButton : MonoBehaviour {

    [SerializeField] private GameObject targetObject;
    [SerializeField] private string targetMessage;
    public Color highlightColor = Color.cyan;

    private SpriteRenderer sprite;

    void Start(){
        sprite = GetComponent<SpriteRenderer>();
    }


    public void OnMouseEnter(){
        sprite.color = highlightColor;
    }

    public void OnMouseExit(){
        sprite.color = Color.white;
    }

    public void OnMouseDown(){
        transform.localScale = new Vector3(1.1f, 1.1f, 1.1f);
    }

    public void OnMouseUp(){
        transform.localScale = Vector3.one;
        targetObject.SendMessage(targetMessage);
    }
}
