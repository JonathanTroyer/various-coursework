﻿using UnityEngine;

[RequireComponent(typeof (SpriteRenderer))]
public class MemoryCard : MonoBehaviour {

    [SerializeField] private GameObject cardBack;
    [SerializeField] private CardController controller;

    public int id{ get; private set; }

    void OnMouseDown () {
        if (cardBack.activeSelf && controller.canFlip) { 
            cardBack.SetActive(false);
            controller.flip(this);
        }
    }

    public void setCard (int id, Sprite sprite) {
        this.id = id;
        GetComponent<SpriteRenderer>().sprite = sprite;
    }

    public void flipBack(){
        cardBack.SetActive(true);
    }
}
