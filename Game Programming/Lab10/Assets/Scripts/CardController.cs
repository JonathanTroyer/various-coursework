﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CardController : MonoBehaviour {

    public const int gridRows = 2;
    public const int gridCols = 4;

    public const float offsetX = 2.0f;
    public const float offsetY = 2.5f;

    public bool canFlip{ get { return secondCard == null; } }

    [SerializeField] private MemoryCard originalCard;
    [SerializeField] private Sprite[] sprites;

    private MemoryCard firstCard, secondCard;
    private int score = 0;

    void Start () {
        Vector3 startPos = originalCard.transform.position;

        int[] ids = shuffleIds(new [] { 0, 0, 1, 1, 2, 2, 3, 3 });


        for (int i = 0; i < gridCols; i++) {
            for (int k = 0; k < gridRows; k++) {
                MemoryCard card = originalCard;
                if (i != 0 || k != 0) {
                    card = Instantiate(originalCard) as MemoryCard;
                }

                int id = ids[k * gridCols + i];
                card.setCard(id, sprites[id]);

                float posX = offsetX * i + startPos.x;
                float posY = -offsetY * k + startPos.y;
                card.transform.position = new Vector3(posX, posY, startPos.z);
            }
        }
    }

    public void Restart(){
        SceneManager.LoadScene("Lab10");
    }

    void OnGUI(){
        int w = Screen.width, h = Screen.height;

        GUIStyle style = new GUIStyle();

        Rect rect = new Rect(15, 15, w, h * 2 / 100);
        style.alignment = TextAnchor.UpperLeft;
        style.fontSize = h * 2 / 50;
        style.normal.textColor = Color.white;
        GUI.Label(rect, "Score: "+score, style);
    }

    public void flip(MemoryCard card){
        if (firstCard == null)
            firstCard = card;
        else {
            secondCard = card;
            StartCoroutine(checkMatch());
        }
    }

    private IEnumerator checkMatch(){
        if (firstCard.id == secondCard.id) {
            score++;
        }
        else {
            yield return new WaitForSeconds(0.5f);

            firstCard.flipBack();
            secondCard.flipBack();
        }
        firstCard = null;
        secondCard = null;
    }

    private int[] shuffleIds (int[] ids) {
        int length = ids.Length;
        for (int i = 0; i < length; i++) {
            int r = Random.Range(0, length);
            int temp = ids[i];
            ids[i] = ids[r];
            ids[r] = temp;
        }

        return ids;
    }
}
