﻿using UnityEngine;

public class Settings : MonoBehaviour {

    public void Open () {
        gameObject.SetActive(true);
    }

    public void Close () {
        gameObject.SetActive(false);
    }

    public void OnSubmitName (string name) {
        
    }

    public void OnSpeedChange (float speed) {
        Messenger<float>.Broadcast(GameEvent.SPEED_CHANGED, speed);
    }
}
