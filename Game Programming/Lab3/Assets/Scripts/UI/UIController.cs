﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour {
    
    [SerializeField] private Text scoreLabel;
    [SerializeField] private Settings settings;

    private int score;

    void Start () {
        settings.Close();
        score = 0;
        scoreLabel.text = score.ToString();
    }

    void Awake () {
        Messenger.AddListener(GameEvent.ENEMY_HIT, OnEnemyHit);
    }

    void OnDestroy(){
        Messenger.RemoveListener(GameEvent.ENEMY_HIT, OnEnemyHit);
    }

    public void OnOpenSettings () {
        settings.Open();
    }

    private void OnEnemyHit () {
        score++;
        scoreLabel.text = score.ToString();
    }
}
