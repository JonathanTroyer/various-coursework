﻿using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;

[RequireComponent(typeof (Camera))]
public class RayShooter : MonoBehaviour {

    private new Camera camera;

    void Start () {
        this.camera = GetComponent<Camera>();

        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    void Update () {
        if (Input.GetMouseButtonDown(0) && !EventSystem.current.IsPointerOverGameObject()) {
            Vector3 center = new Vector3(this.camera.pixelWidth / 2, this.camera.pixelHeight / 2, 0);
            Ray ray = this.camera.ScreenPointToRay(center);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit)) {
                GameObject hitObject = hit.transform.gameObject;
                ReactiveTarget target = hitObject.GetComponent<ReactiveTarget>();
                if (target != null) {
                    target.ReactToHit();
                    Messenger.Broadcast(GameEvent.ENEMY_HIT);
                }
                else {
                    StartCoroutine(SphereIndicator(hit.point));
                }
            }
        }
    }

    private IEnumerator SphereIndicator (Vector3 point) {
        GameObject sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        sphere.transform.position = point;

        yield return new WaitForSeconds(1);

        Destroy(sphere);
    }
}
