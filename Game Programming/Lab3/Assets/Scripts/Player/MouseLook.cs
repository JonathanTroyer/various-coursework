﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MouseLook : MonoBehaviour {
    public enum RotationAxes
    {
        MouseXAndY,
        MouseX,
        MouseY
    }

    public RotationAxes axes = RotationAxes.MouseXAndY;

    public float sensHorz = 9.0f;
    public float sensVert = 9.0f;

    public float minVert = -90.0f;
    public float maxVert = 90.0f;

    private float _rotationX = 0;

    void Start () {
        Rigidbody body = GetComponent<Rigidbody>();
        if (body != null) {
            body.freezeRotation = true;
        }
    }

    void LateUpdate () {
        if (!EventSystem.current.IsPointerOverGameObject()) {
            if (axes == RotationAxes.MouseX) {
                transform.Rotate(0, Input.GetAxisRaw("Mouse X") * sensHorz, 0);
            }
            else if (axes == RotationAxes.MouseY) {
                updateRotX();
                transform.localEulerAngles = new Vector3(_rotationX, transform.localEulerAngles.y, 0);
            }
            else {
                updateRotX();
                float delta = Input.GetAxisRaw("Mouse X") * sensHorz;
                float rotationY = transform.localEulerAngles.y + delta;

                transform.localEulerAngles = new Vector3(_rotationX, rotationY, 0);
                Debug.Log("Rotation x: " + _rotationX);
                Debug.Log("Local Euler angles: " + transform.localEulerAngles);
            }
        }

    }

    private void updateRotX () {
        _rotationX -= Input.GetAxisRaw("Mouse Y") * sensVert;
        _rotationX = Mathf.Clamp(_rotationX, minVert, maxVert);
    }
}
