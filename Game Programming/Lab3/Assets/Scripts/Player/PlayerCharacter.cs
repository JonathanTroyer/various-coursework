﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCharacter : MonoBehaviour {

    public int health;

    public void damage (int amount) {
        health -= amount;
        Debug.Log("Health: " + health);
    }
}
