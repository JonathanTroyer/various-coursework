﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof (CharacterController))]
public class FPSInput : MonoBehaviour {
	
    public float baseSpeed = 6.0f;
    private float speed;
    public float gravity = -9.8f;

    private CharacterController controller;

    void Start () {
        controller = GetComponent<CharacterController>();
        speed = baseSpeed;
    }

    void Awake () {
        Messenger<float>.AddListener(GameEvent.SPEED_CHANGED, OnSpeedChanged);
    }

    void OnDestroy () {
        Messenger<float>.RemoveListener(GameEvent.SPEED_CHANGED, OnSpeedChanged);
    }

    void Update () {
        Vector3 movement = new Vector3(Input.GetAxis("Horizontal") * speed, 0, Input.GetAxis("Vertical") * speed);
        movement = Vector3.ClampMagnitude(movement, speed);
        movement.y = gravity;
        movement = transform.TransformDirection(movement);
        controller.Move(movement * Time.deltaTime);

        if (Input.GetKeyDown(KeyCode.Escape)) {
            Cursor.visible = !Cursor.visible;
        }
    }

    private void OnSpeedChanged (float val) {
        speed = baseSpeed * val;
    }
}
