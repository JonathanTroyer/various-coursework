﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wander : MonoBehaviour {

    public float baseSpeed = 3.0f;
    private float speed;
    public float range = 5.0f;

    private bool isAlive = true;

    [SerializeField] private GameObject fireballType;
    private GameObject fireball;

    void Start () {
        speed = baseSpeed;
    }

    void Awake () {
        Messenger<float>.AddListener(GameEvent.SPEED_CHANGED, OnSpeedChanged);
    }

    void OnDestroy () {
        Messenger<float>.RemoveListener(GameEvent.SPEED_CHANGED, OnSpeedChanged);
    }

    void Update () {
        if (isAlive) {
            this.transform.Translate(0, 0, speed * Time.deltaTime);
        }

        Ray ray = new Ray(transform.position, transform.forward);
        RaycastHit hit;
        if (Physics.SphereCast(ray, 0.75f, out hit)) {
            GameObject hitObject = hit.transform.gameObject;
            if (hitObject.GetComponent<PlayerCharacter>()) {
                if (fireball == null) {
                    fireball = Instantiate(fireballType) as GameObject;
                    fireball.transform.position = this.transform.TransformPoint(transform.forward * 1.5f);
                    fireball.transform.rotation = this.transform.rotation;
                }
            }
            if (hit.distance < range) {
                this.transform.Rotate(0, Random.Range(-100, 100), 0);
            }
        }
    }

    public void setAlive (bool alive) {
        isAlive = alive;
    }

    private void OnSpeedChanged (float val) {
        speed = baseSpeed * val;
    }
}
