﻿using System.Collections;
using UnityEngine;

public class ReactiveTarget : MonoBehaviour {

    public void ReactToHit () {
        Wander ai = GetComponent<Wander>();
        if (ai != null) {
            ai.setAlive(false);
        }
        StartCoroutine(Die());
    }

    private IEnumerator Die () {
        this.transform.Rotate(-75, 0, 0);
        yield return new WaitForSeconds(1.5f);

        Destroy(this.gameObject);
    }
}
