﻿using System.Collections;
using UnityEngine;

[RequireComponent(typeof (Rigidbody))]
public class Fireball : MonoBehaviour {

	public float speed = 10.0f;
	public int damage = 1;

	private Rigidbody body;

	void Start () {
		body = GetComponent<Rigidbody>();
		body.velocity = transform.forward * speed;
	}

	void OnTriggerEnter (Collider other) {
		PlayerCharacter player = other.GetComponent<PlayerCharacter>();
		if (player != null) {
			player.damage(damage);
		}
		Destroy(this.gameObject);
	}
}
