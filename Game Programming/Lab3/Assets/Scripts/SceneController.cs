﻿using System.Collections;
using UnityEngine;

public class SceneController : MonoBehaviour {
    public int numberOfEnemies = 10;

    [SerializeField] private GameObject enemyType;
    private GameObject[] enemies;

    void Start () {
        enemies = new GameObject[numberOfEnemies];
    }

    void Update () {
        for (int i = 0; i < numberOfEnemies; i++) {
            GameObject enemy = enemies[i];
            if (enemy == null) {
                enemy = Instantiate(enemyType) as GameObject;
                enemy.transform.position = this.transform.position;
                enemy.transform.Rotate(0, Random.Range(0, 360), 0);
            }
            enemies[i] = enemy;
        }
    }

}
