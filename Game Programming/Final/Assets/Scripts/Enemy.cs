﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Enemy : Droid {

	new void Start() {
		base.Start();

		StartCoroutine(updateDroids());
	}

	//Find the nearest ally and attempt to hunt him down and murder him
	void Update() {
		if (foundDroids != null && foundDroids.Count() > 0) {
			Vector3 pos = getClosestDroidPos();
			//Hun him down using the nav mesh
			agent.SetDestination(pos);
			transform.LookAt(pos);
			target = pos;
		} else {
			target = Vector3.zero;
		}

		//Try to murder him
		if (target != Vector3.zero && Vector3.Distance(transform.position, target) < throwDistance && canThrow)
			throwDeathball();
	}

	//Find the nearest droids which are allies
	protected IEnumerator updateDroids() {
		base.findDroids();
		if (foundDroids != null) {
			foundDroids = foundDroids.Where(x => x.tag == "Ally");
		}

		yield return new WaitForSeconds(updateTime);
		StartCoroutine(updateDroids());
	}

	//A special function to give enemies unlimited ammunition
	new public void throwDeathball() {
		if (canThrow) {
			var deathball = Instantiate(deathballType);
			deathball.SetActive(false);
			deathballs.Push(deathball);
			base.throwDeathball();
		}
	}

	//Tell the player we got murdered (good)
	void OnDestroy() {
		MessageQueue.Inform(this.gameObject.name + " (enemy) destroyed at " + transform.position.ToString());
	}
}
