﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;

//Droid is the base class for the two main actors in the game

[RequireComponent(typeof(NavMeshAgent))]
public class Droid : MonoBehaviour {

	[SerializeField] protected GameObject deathballType;

	public float strength = 100.0f;
	public float lineOfSight = 20.0f;
	public float throwDistance = 10.0f;
	public float throwTime = 5.0f;
	protected bool canThrow = false; //Initially set to false to avoid a glitch where enemies commit suicide
	public float updateTime = 5.0f;

	protected Stack<GameObject> deathballs;
	protected IEnumerable<Droid> foundDroids;
	protected Vector3 target;
	protected NavMeshAgent agent;

	//Initialize variables
	protected void Start() {
		deathballs = new Stack<GameObject>();
		agent = GetComponent<NavMeshAgent>();
		StartCoroutine(updateThrow());
	}

	//A function to find other droids near this one
	protected void findDroids() {
		var hits = Physics.SphereCastAll(new Ray(transform.position, Vector3.forward), lineOfSight, 0.0f, 1 << 8);
		//SphereCastAll returns an array, which may be null. Why they didn't just return an empty array, we'll never know.
		if (hits != null) {
			var hitTransforms = hits.Select(x => x.transform);
			//For whatever reason the sphere cast can return things with null game objects, so this is included as a separate step to avoid that
			var hitsObjs = hitTransforms.Where(x => x != null).Select(x => x.gameObject);
			foundDroids = hitsObjs.Select(x => x.GetComponent<Droid>());
		}
	}

	//Defer to exposed collision function
	void OnCollisionEnter(Collision col) {
		handleCollision(col);
	}

	//Expose the collision function to the outside world
	//This is neccessary due to the model being two pieces (see DroidBits)
	public void handleCollision(Collision col) {
		if (col.gameObject.tag == "Deathball") {
			var deathball = col.gameObject;
			if (deathball.GetComponent<ParticleSystem>().IsAlive()) {
				Destroy(this.gameObject);
			} else {
				deathballs.Push(deathball);
				deathball.SetActive(false);
			}
		}
	}

	//Throw the deathball if they have one and haven't thrown in a while
	public void throwDeathball() {
		if (canThrow && deathballs.Count > 0) {
			canThrow = false; //Set immediately to avoid delay introduced by StartCoroutine, otherwise you'd get large amounts of spam
			StartCoroutine(updateThrow());
			//Get it and set it up
			var deathball = deathballs.Pop();
			deathball.SetActive(true);
			var pos = transform.position;
			pos += transform.forward * 1.5f;
			deathball.transform.position = pos;
			//Throw it
			var rigidbody = deathball.GetComponent<Rigidbody>();
			rigidbody.AddForce(transform.forward * strength, ForceMode.Impulse);
		}
	}

	//Get the position of the closest droid that we've found
	//It's just a linear minimum search
	protected Vector3 getClosestDroidPos() {
		Vector3 pos = transform.position;
		float minDistance = float.MaxValue;
		foreach(Droid d in foundDroids) {
			var distance = Vector3.Distance(transform.position, d.transform.position);
			if (d != null && distance < minDistance) {
				pos = d.transform.position;
				minDistance = distance;
			}
		}

		return pos;
	}

	//Use coroutines to basically debounce throwing deathballs
	private IEnumerator updateThrow() {
		yield return new WaitForSeconds(throwTime);
		canThrow = true;
	}

}