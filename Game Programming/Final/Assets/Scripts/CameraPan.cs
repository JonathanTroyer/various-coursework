﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//WOO PHYSICS-BASED CAMERAS!!!

[RequireComponent(typeof(Rigidbody))]
public class CameraPan : MonoBehaviour {

	public float speed = 5.0f;

	private Rigidbody body;
	private Vector3 movement;

	//Get the camera and lock the cursor
	void Start() {
		this.body = GetComponent<Rigidbody>();
		Cursor.lockState = CursorLockMode.Confined;
	}

	void Update() {
		//Physics-based pan
		var vert = Input.GetAxis("Vertical");
		var horz = Input.GetAxis("Horizontal");
		Vector3 move = new Vector3(horz, 0, vert);

		//Detect if the mouse is near the edge of the screen +- 10 units
		if (Input.mousePosition.x >= Screen.width - 10)
			move.x++;
		else if (Input.mousePosition.x <= 10)
			move.x--;

		if (Input.mousePosition.y >= Screen.height - 10)
			move.z++;
		else if (Input.mousePosition.y <= 10)
			move.z--;

		//Move the camera faster when we're high up so it seems constant
		var height = transform.position.y;
		movement = Vector3.ClampMagnitude(move, height / 10.0f) * speed * height / 10.0f;

		//Snap zoom
		var zoom = Input.GetAxis("Mouse ScrollWheel");
		this.transform.position += new Vector3(0, -zoom, 0) * 10;
	}

	void FixedUpdate() {
		body.AddForce(movement);
	}
}
