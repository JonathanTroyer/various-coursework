﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Ally : Droid {

	public bool selected = true;
	[SerializeField] Camera mainCam;

	new void Start() {
		base.Start();
		//Start us off with 10 deathballs
		for(int i = 0; i < 10; i++) {
			var deathball = Instantiate(deathballType);
			deathball.SetActive(false);
			deathballs.Push(deathball);
		}
		StartCoroutine(updateDroids());
	}

	//On each frame, attempt to murder the closest droid
	void Update() {
		//Find a target
		if (foundDroids != null && foundDroids.Count() > 0) {
			Vector3 pos = getClosestDroidPos();
			transform.LookAt(pos);
			target = pos;
		} else {
			target = Vector3.zero;
		}

		//Try to murder him
		if (target != Vector3.zero && Vector3.Distance(transform.position, target) < throwDistance && canThrow)
			base.throwDeathball();

		//Allow players to move the allies using right-click, similar to many other RTS games
		if (Input.GetMouseButtonDown(1)) {
			RaycastHit hit;
			if (Physics.Raycast(mainCam.ScreenPointToRay(Input.mousePosition), out hit))
				agent.SetDestination(hit.point);
		}
	}

	//Find droids near us that are enemies
	protected IEnumerator updateDroids() {
		base.findDroids();
		if (foundDroids != null) {
			foundDroids = foundDroids.Where(x => x.tag == "Enemy");
		}

		yield return new WaitForSeconds(updateTime);
		StartCoroutine(updateDroids());
	}

	//Tell the player we got murdered (bad)
	void OnDestroy() {
		MessageQueue.Alert(this.gameObject.name + " (ally) destroyed at " + transform.position.ToString());
	}

}