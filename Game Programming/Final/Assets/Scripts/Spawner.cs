﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//A simple script to continuously spawn a GameObject in a square area

public class Spawner : MonoBehaviour {

	public float spawnRate = 15.0f;
	public float increaseRate = 10.0f;
	public float minSpawnRate = 2.0f;
	[SerializeField] GameObject spawnType;
	public float spawnArea = 50.0f;


	void Start() {
		StartCoroutine(spawn());
	}

	IEnumerator spawn() {
		var spawnObj = Instantiate(spawnType);
		spawnObj.transform.position = new Vector3(Random.value, 0, Random.value) * spawnArea;
		MessageQueue.Warn("Enemy created at " + spawnObj.transform.position);
		yield return new WaitForSeconds(spawnRate);
		StartCoroutine(spawn());
	}

	void Update() {
		spawnRate -= Time.deltaTime/increaseRate;
		if (spawnRate < minSpawnRate)
			spawnRate = minSpawnRate;
	}

}
