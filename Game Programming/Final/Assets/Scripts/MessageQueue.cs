﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//A simple global notification UI component, similar to Leauge of Legends and various MMO's

[RequireComponent(typeof(Text))]
public class MessageQueue : MonoBehaviour {

	[SerializeField] static float messageTime = 10.0f;

	private static Text textBox;
	private static Queue<string> messages;

	private static MessageQueue instance;

	protected MessageQueue() {}

	void Awake() {
		instance = this;
		textBox = GetComponent<Text>();
		messages = new Queue<string>();
	}

	public static void Alert(string message) {
		addMessage(message, "red");
	}

	public static void Warn(string message) {
		addMessage(message, "yellow");
	}

	public static void Inform(string message) {
		addMessage(message, "green");
	}

	public static void Message(string message) {
		addMessage(message, "cyan");
	}

	private static void addMessage(string message, string color) {
		instance.StartCoroutine(instance.addToQueue("<color=" + color + ">" + message + "</color>"));
	}

	//We use a queue to ensure the player sees each message
	private IEnumerator addToQueue(string message) {
		messages.Enqueue(message);
		updateMessages();
		yield return new WaitForSeconds(messageTime);
		messages.Dequeue();
		updateMessages();
	}

	//Display all the messages
	private static void updateMessages() {
		string allMessages = "";
		foreach (string message in messages) {
			allMessages += message + "\n";
		}
		textBox.text = allMessages;
	}

}
