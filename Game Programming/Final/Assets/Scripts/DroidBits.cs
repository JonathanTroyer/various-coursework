﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DroidBits : MonoBehaviour {

	private Droid parent;

	void Awake() {
		parent = transform.parent.GetComponent<Droid>();
	}

	//All we do is tie our collisions to the parent
	void OnCollisionEnter(Collision col) {
		if (parent != null)
			parent.handleCollision(col);
	}
}
