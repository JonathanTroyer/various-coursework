﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Spawn : MonoBehaviour {

    public Vector2 size;
    public float time = 40.0f;

    [SerializeField] private Text countText;
    [SerializeField] private Text timerText;
    [SerializeField] private PhysicsControl player;
    [SerializeField] private GameObject spawnType;

    private GameObject lastSpawn;
    private int count = 0;

    void Start() {
        countText.text = count.ToString();
        timerText.text = time.ToString();
        StartCoroutine(TimedSpawn());
    }

    void Update() {
        time -= Time.deltaTime;
        if (time > 0) {
            timerText.text = string.Format("{0:0.00}", time);
        }
        else {
            timerText.text = "Game Over";
            player.canMove = false;
        }
    }

    IEnumerator TimedSpawn() {
        var spawn = Instantiate(spawnType) as GameObject;
        spawn.transform.position = new Vector3(Random.Range(-size.x, size.x), 1, Random.Range(-size.y, size.y));
        var controller = spawn.GetComponent<CollisionController>();
        if (controller != null) {
            controller.spawner = this;
        }
        lastSpawn = spawn;

        yield return new WaitForSeconds(4);
        Destroy(lastSpawn);

        if (time > 0)
            StartCoroutine(TimedSpawn());
    }

    public void increaseCount() {
        count++;
        countText.text = count.ToString();
    }
}
