﻿using System.Collections;
using UnityEngine;

[RequireComponent(typeof (Renderer))]
public class CollisionController : MonoBehaviour {

    public Material switchTo;
    public Spawn spawner;

    private Renderer myRenderer;
    private bool collided = false;

    void Start() {
        myRenderer = GetComponent<Renderer>();	
    }

    void OnCollisionEnter() {
        if (!collided) {
            collided = true;
            myRenderer.material = switchTo;
            spawner.increaseCount();
            StartCoroutine(Die());
        }
    }

    IEnumerator Die() {
        yield return new WaitForSeconds(1);
        Destroy(this.gameObject);
    }
}
