﻿using System.Collections;
using UnityEngine;

[RequireComponent(typeof (Rigidbody))]
public class PhysicsControl : MonoBehaviour {

    public float speed = 5.0f;
    public bool canMove = true;

    private Rigidbody myBody;

    void Start() {
        myBody = GetComponent<Rigidbody>();
    }

    void Update() {
        if (canMove) {
            float inHorz = Input.GetAxis("Horizontal");
            float inVert = Input.GetAxis("Vertical");

            Vector3 move = new Vector3(inHorz, 0, inVert);

            myBody.AddForce(move * speed);
        }
        else {
            myBody.velocity = Vector3.zero;
        }
    }
}
