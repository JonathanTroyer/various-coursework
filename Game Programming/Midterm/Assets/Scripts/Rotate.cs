﻿using UnityEngine;

public class Rotate : MonoBehaviour {

    public float rotationSpeed = 1.0f;

    void Update() {
        transform.Rotate(Vector3.one * 360 * Time.deltaTime * rotationSpeed);
    }
}
