﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeController : MonoBehaviour {

	public float velocity;
	public float rotationSize;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void FixedUpdate(){
		transform.Translate (velocity*Time.deltaTime, 0, 0);
		transform.Rotate (0, 0, velocity/rotationSize);
	}
}
