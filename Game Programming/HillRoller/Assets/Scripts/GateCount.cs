﻿using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System.Collections.Generic;


[RequireComponent(typeof(Text))]
public class GateCount : MonoBehaviour {

    private Text gateLabel;

    void Start() {
        gateLabel = GetComponent<Text>();
        gateLabel.color = Color.red;
    }

    public void updateGates(IEnumerable<Gate> gates) {
        int gatesLeft = gates.Count(x => !x.getPassedThrough());

        gateLabel.text = string.Format("Gates left: {0:0} out of {1:0}", gatesLeft, gates.Count());
        if (gatesLeft == 0)
            gateLabel.color = Color.green;
        else
            gateLabel.color = Color.red;
    }
}
