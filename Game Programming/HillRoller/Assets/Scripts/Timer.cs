﻿using UnityEngine.UI;
using UnityEngine;
using System.Collections;

[RequireComponent(typeof (Text))]
public class Timer : MonoBehaviour {

    public float countdown = 3.0f;

    private Text timerLabel;

    void Start() {
        timerLabel = GetComponent<Text>();
    }

    void Update() {
        timerLabel.text = string.Format("{0:#.0}", Mathf.Abs(countdown -= Time.deltaTime));
    }
}
