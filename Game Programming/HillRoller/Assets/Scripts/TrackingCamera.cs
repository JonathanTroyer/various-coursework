﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrackingCamera : MonoBehaviour {

    public GameObject tracking;
    public Vector3 offset;

    void LateUpdate() {
        this.transform.position = tracking.transform.position + offset;
    }
}
