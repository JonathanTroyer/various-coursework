﻿using System.Collections;
using UnityEngine;

[RequireComponent(typeof (Collider))]
public class Repeater : MonoBehaviour {

    public SceneController controller;
    public Material originalMaterial;
    public Material newMaterial;

    public bool canRepeat = false;

    void OnCollisionEnter() {
        controller.Repeat();
    }

    public void swapMaterial() {
        Renderer r = GetComponent<Renderer>();
        if (r != null)
            r.material = newMaterial;
    }

    public void resetMaterial(){
        Renderer r = GetComponent<Renderer>();
        if (r != null)
            r.material = originalMaterial;
    }
}
