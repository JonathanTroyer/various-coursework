﻿using System.Collections;
using UnityEngine;
using System.Linq;
using System;

public class SceneController : MonoBehaviour {

    public float difficulty;
    public float time = 3.0f;
    public Timer timer;
    public GateCount gateCount;

    [SerializeField] private GameObject gateBase;
    private GameObject[] gates;
    [SerializeField] private GameObject bumperBase;
    private GameObject[] bumpers;

    [SerializeField] private GameObject playerType;
    private GameObject player;
    public Repeater repeater;
    [SerializeField] private GameObject stopperType;

    [SerializeField] private new TrackingCamera camera;

    private float startPos = -4;
    private float endPos = 5;

    void Start() {
        this.reset();
    }

    public void Repeat() {
        if (complete()) {
            difficulty++;
            Destroy(player);
            foreach (GameObject b in bumpers) {
                Destroy(b);
            }
            foreach (GameObject g in gates) {
                Destroy(g);
            }
            reset();
        }
    }

    private void reset() {
        transform.parent.localScale = new Vector3(1, 1, difficulty);

        player = Instantiate(playerType) as GameObject;
        var pos = this.transform.position;
        player.transform.position = new Vector3(pos.x, pos.y + difficulty/(difficulty-1), pos.z);

        this.camera.tracking = player;

        repeater.resetMaterial();
        this.createGates();
        this.createBarriers();
        this.updateGates();

        StartCoroutine(removeBarrier());
    }

    private void createGates() {
        gates = new GameObject[Mathf.Max((int) difficulty - 2, 4)];
        this.spread(ref gates, gateBase);
        foreach (GameObject gate in gates) {
            gate.AddComponent<Gate>();
            var pos = gate.transform.localPosition;
            gate.transform.localPosition = new Vector3(UnityEngine.Random.Range(-3, 3), pos.y, pos.z);
        }
    }

    public void updateGates() {
        gateCount.updateGates(gates.Select(g => g.GetComponent<Gate>()));
        if (complete()) {
            repeater.swapMaterial();
        }
    }

    public bool complete() {
        return gates.Select(g => g.GetComponent<Gate>()).Count(g => !g.getPassedThrough()) == 0;
    }

    private void createBarriers() {
        bumpers = new GameObject[Mathf.Max((int) difficulty - 5, 3)];
        this.spread(ref bumpers, bumperBase);
        foreach (GameObject bumper in bumpers) {
            bumper.AddComponent<Bumper>();
        }
    }

    private void spread(ref GameObject[] gos, GameObject type) {
        var scale = (endPos - startPos) / gos.Length;

        for (int i = 0; i < gos.Length; i++) {
            var go = Instantiate(type) as GameObject;
            go.transform.SetParent(this.transform.parent.transform);

            var pos = Vector3.up;
            pos.z = startPos + scale * i;
            go.transform.localPosition = pos;
            go.transform.localRotation = this.transform.localRotation;

            gos[i] = go;
        }
    }

    private IEnumerator removeBarrier() {
        timer.countdown = time;
        var stopper = Instantiate(stopperType) as GameObject;
        stopper.transform.SetParent(this.transform);
        stopper.transform.localPosition = new Vector3(0, 0, 0.125f);
        stopper.transform.localEulerAngles = new Vector3(-90, 0, 0);
        stopper.transform.localScale = new Vector3(1, 1, 0.25f);
        yield return new WaitForSeconds(time);

        foreach (Transform child in this.transform) {
            Destroy(child.gameObject);
        }
    }
}
