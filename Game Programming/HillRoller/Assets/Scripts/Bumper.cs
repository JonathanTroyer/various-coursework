﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof (Rigidbody))]
public class Bumper : MonoBehaviour {
	
    public float speed = 1;

    private bool direction { get; set; }

    private new Rigidbody rigidbody;

    void Start() {
        
    }

    void Update() {
        if (direction)
            this.transform.position += Vector3.right * Time.deltaTime;
        else
            this.transform.position += Vector3.left * Time.deltaTime;

        if (this.transform.position.x >= 2.75)
            direction = false;
        else if (this.transform.position.x <= -2.75)
            direction = true;
    }
}
