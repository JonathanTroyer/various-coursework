﻿using UnityEngine;

public class Gate : MonoBehaviour {

    public Material material;

    private bool passedThrough = false;

    void OnTriggerExit(Collider other) {
        PlayerController controller = other.GetComponent<PlayerController>();
        if (controller != null) {
            this.passedThrough = true;

            var children = GetComponentsInChildren<Renderer>();
            foreach (Renderer r in children) {
                r.material = this.material;
            }

            var sceneControllers = transform.parent.GetComponentsInChildren<SceneController>();
            foreach (SceneController sc in sceneControllers) {
                sc.updateGates();
            }
        }
    }

    public bool getPassedThrough() {
        return this.passedThrough;
    }
}
