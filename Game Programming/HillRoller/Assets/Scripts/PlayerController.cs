﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof (Rigidbody))]
public class PlayerController : MonoBehaviour {

    public float speed = 1;

    private new Rigidbody rigidbody;
    private float moveHorizontal;
    private float moveVertical;

    void Start() {
        this.rigidbody = GetComponent<Rigidbody>();
    }

    void Update() {
        this.moveHorizontal = Input.GetAxis("Horizontal");
        this.moveVertical = Input.GetAxis("Vertical");
    }

    void FixedUpdate() {
        Vector3 force = new Vector3(this.moveHorizontal, 0.0f, this.moveVertical);
        this.rigidbody.AddForce(force * speed);
    }
}
