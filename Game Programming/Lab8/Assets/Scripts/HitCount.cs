﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitCount : MonoBehaviour {

    private int count = 0;

    void OnGUI () {
        int w = Screen.width, h = Screen.height;

        GUIStyle style = new GUIStyle();

        Rect rect = new Rect(0, 0, w, h);
        style.alignment = TextAnchor.UpperRight;
        style.fontSize = h * 2 / 50;
        style.normal.textColor = Color.green;
        string text = "Hit: " + count;
        GUI.Label(rect, text, style);
    }

    public void increaseCount () {
        count++;
    }
}
