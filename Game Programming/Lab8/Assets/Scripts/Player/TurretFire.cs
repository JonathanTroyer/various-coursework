﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretFire : MonoBehaviour {

    public float force = 5.0f;
    public float fireRate = 0.5f;

    private bool firing = false;

    [SerializeField] private GameObject ammunitionType;

    void Update () {
        if (Input.GetKeyDown(KeyCode.Space)) {
            firing = true;
            StartCoroutine(AutoFire());
        }
        if (Input.GetKeyUp(KeyCode.Space))
            firing = false;
    }

    private IEnumerator AutoFire () {
        while (firing) {
            var ammunition = Instantiate(ammunitionType) as GameObject;
            ammunition.transform.position = transform.position;
            var ammoBody = ammunition.AddComponent<Rigidbody>();
            ammoBody.mass = 500;
            ammoBody.AddForce(transform.up * force);

            yield return new WaitForSeconds(fireRate);
        }
    }
}
