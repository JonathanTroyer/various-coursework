﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ammunition : MonoBehaviour {

    void Start () {
        StartCoroutine(Despawn());
    }

    void OnCollisionEnter (Collision target) {
        var wanderer = target.gameObject.GetComponent<Wander>();
        if (wanderer != null) {
            wanderer.ReactToHit();
        }
    }

    private IEnumerator Despawn () {
        yield return new WaitForSeconds(3);
        Destroy(this.gameObject);
    }
}
