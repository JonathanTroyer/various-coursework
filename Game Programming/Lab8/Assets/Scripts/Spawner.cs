﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof (HitCount))]
public class Spawner : MonoBehaviour {

    public int numberOfEnemies = 1;

    [SerializeField] private GameObject spawnType;
    private GameObject[] spawns;
    private bool[] counted;

    private HitCount counter;

    void Start () {
        spawns = new GameObject[numberOfEnemies];
        counted = new bool[numberOfEnemies];
        counter = GetComponent<HitCount>();
        for (int i = 0; i < numberOfEnemies; i++) {
            GameObject enemy = spawns[i];
            if (enemy == null) {
                enemy = this.createSpawn();
                counted[i] = false;
            }
            spawns[i] = enemy;
        }
    }

    void Update () {
        for (int i = 0; i < numberOfEnemies; i++) {
            GameObject enemy = spawns[i];

            if (enemy == null) {
                enemy = this.createSpawn();
                counted[i] = false;
            }
            else {
                Wander wanderer = enemy.GetComponent<Wander>();
                if (wanderer != null && !wanderer.isAlive() && !counted[i]) {
                    counter.increaseCount();
                    counted[i] = true;
                }
            }
            spawns[i] = enemy;
        }
    }

    GameObject createSpawn () {
        var spawn = Instantiate(spawnType) as GameObject;
        spawn.transform.position = this.transform.position;
        spawn.transform.Rotate(0, Random.Range(0, 360), 0);
        return spawn;
    }
}
