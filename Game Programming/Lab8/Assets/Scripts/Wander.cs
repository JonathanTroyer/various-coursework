﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof (Rigidbody))]
public class Wander : MonoBehaviour {

    public float speed = 3.0f;
    public float rotationSpeed = 2f;
    public float range = 1.0f;

    private bool alive = true;
    private bool isRotating = false;
    private Rigidbody body;
    private Vector3 targetRotation = Vector3.zero;

    void Start () {
        body = GetComponent<Rigidbody>();
    }

    void Update () {
        if (alive) {
            //If we're not turning away from a wall, check for walls
            RaycastHit hit;
            if (!isRotating && body.SweepTest(transform.forward, out hit, range)) {
                //We've seen something, so get setup to turn away from it.
                isRotating = true;
                targetRotation = new Vector3(Random.Range(-1.0f, 1.0f), 0, Random.Range(-1.0f, 1.0f));
            }

            //We're currently turning
            if (isRotating) {
                //Stop moving
                body.velocity = Vector3.zero;
                //Get our new angle (for smooth turning) and turn towards it
                Vector3 newRotation = Vector3.RotateTowards(transform.forward, targetRotation, rotationSpeed * Time.deltaTime, targetRotation.magnitude);
                body.rotation = Quaternion.LookRotation(newRotation);
                //Check if we're at the final rotation
                Vector3 crossProduct = Vector3.Cross(newRotation, targetRotation);
                if (crossProduct == Vector3.zero) {
                    isRotating = false;
                }

            }
            else {
                //Start moving
                body.velocity = transform.forward * speed;
            }
        }
    }

    public void ReactToHit () {
        alive = false;
        //Go "limp"
        body.constraints = RigidbodyConstraints.None;
        body.useGravity = true;
        //Turn on the fire
        var particles = GetComponentsInChildren(typeof(ParticleSystem));
        foreach (ParticleSystem particle in particles) {
            particle.Play();
        }
		var ring = transform.Find ("Float");
		if (ring != null) {
			Destroy (ring.gameObject);
		}

        StartCoroutine(Die());
    }

    public bool isAlive () {
        return alive;
    }

    private IEnumerator Die () {
        //body.velocity = Vector3.zero;
        yield return new WaitForSeconds(2);

        Destroy(this.gameObject);
    }
}
