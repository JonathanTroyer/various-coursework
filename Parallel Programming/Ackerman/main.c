#include <stdio.h>
#include <time.h>

unsigned long long calls = 0;

unsigned long long ackerman(unsigned long long x, unsigned long long y) {
    calls++;
    if(x == 0) {
        return y+1;
    } else if (y == 0) {
        return ackerman(x - 1, 1);
    } else {
        return ackerman(x - 1, ackerman(x, y - 1));
    }
}

int main() {
    unsigned long long x = 3, y = 15;
    clock_t begin = clock();
    unsigned long long result =  ackerman(x, y);
    clock_t end = clock();
    double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
    printf("Ackerman(%lli, %lli) did %lli calls in %f seconds, giving in %lli", x, y, calls, time_spent, result);
    return 0;
}

