﻿using System;
using System.Threading.Tasks;

namespace ConsoleApplication
{
    public class Treewalk
    {
        static void Main(string[] args)
        {
            Tree<MyClass> tree = new Tree<MyClass>();
            //… populate tree here (you don’t need to do this part)
            fillTree(tree, 4);

            // Define the action to perform on each node
            Action<MyClass> myAction = x => Console.WriteLine("{0} :  {1}", x.Name, x.Number);

            // Traverse the tree with parallel tasks
            DoTree(tree, myAction);
        }

        static void fillTree(Tree<MyClass> tree, int depth)
        {
            tree.Data = new MyClass(tree.ToString(), depth);
            if (depth != 0)
            {
                tree.Left = new Tree<MyClass>();
                fillTree(tree.Left, depth-1);
                tree.Right = new Tree<MyClass>();
                fillTree(tree.Right, depth-1);
            }
        }

        public class MyClass
        {
            public string Name { get; set; }
            public int Number { get; set; }

            public MyClass(string name, int number)
            {
                Name = name;
                Number = number;
            }
        }

        public class Tree<T>
        {
            public Tree<T> Left;
            public Tree<T> Right;
            public T Data;
        }

        // By using tasks explicitly.
        public static void DoTree<T>(Tree<T> tree, Action<T> action)
        {
            if (tree == null) return;
            var left = Task.Factory.StartNew(() => DoTree(tree.Left, action));
            var right = Task.Factory.StartNew(() => DoTree(tree.Right, action));
            action(tree.Data);
            try
            {
                Task.WaitAll(left, right);
            }
            catch (AggregateException)
            {
                // Handle exception here (you need not do this either but do explain what it is for)
            }
        }

        // By using Parallel.Invoke (which is a pragma to incur parallel task execution)
        public static void DoTree2<T>(Tree<T> tree, Action<T> action)
        {
            if (tree == null) return;
            Parallel.Invoke(
                () => DoTree2(tree.Left, action),
                () => DoTree2(tree.Right, action),
                () => action(tree.Data)
            );
        }
    }
}