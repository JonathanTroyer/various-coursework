#include <stdlib.h>
#include <stdio.h>
#include <omp.h>
#include <time.h>
#include <sys/time.h>

double getRand() {
    return (double) rand() * 2 / (double) (RAND_MAX) - 1;
}

double PI(long long number_of_tosses) {
    unsigned long long number_in_circle = 0;
    for (long long toss = 0; toss < number_of_tosses; toss++) {
        double x = getRand(), y = getRand();
        double dist_squared = x * x + y * y;
        number_in_circle += dist_squared <= 1;
    }

    return 4 * number_in_circle / (double) number_of_tosses;
}

double PI_p(long long number_of_tosses) {
    unsigned long long number_in_circle = 0;
#   pragma omp parallel for num_threads(4) \
        reduction(+:number_in_circle)
    for (long long toss = 0; toss < number_of_tosses; toss++) {
        double x = getRand(), y = getRand();
        double dist_squared = x * x + y * y;
        number_in_circle += dist_squared <= 1;
    }

    return 4 * number_in_circle / (double) number_of_tosses;
}

int main() {
    int iterations = 10;
    double avg_time_p;
    struct timeval start, end;

    printf("Averaging sequential...\n");
    gettimeofday(&start, NULL);
    for (int i = 0; i < iterations; i++) {
        PI(random() / 2);
    }
    gettimeofday(&end, NULL);
    printf("Took %f seconds\n\n", end.tv_sec - start.tv_sec + (double) (end.tv_usec - start.tv_usec) / 1000000);

    printf("Averaging parallel...\n");
    gettimeofday(&start, NULL);
    for (int i = 0; i < iterations; i++) {
        PI_p(random() / 2);
    }
    gettimeofday(&end, NULL);
    printf("Took %f seconds\n\n", end.tv_sec - start.tv_sec + (double) (end.tv_usec - start.tv_usec) / 1000000);

    printf("\nπ = %f", PI_p(1000000000000));

    return 0;
}

