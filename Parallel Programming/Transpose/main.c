#include <stdio.h>

int main() {
    int r, c;
    printf("Enter the matrix size: ");
    scanf("%i,%i", &c, &r);
    int matrix[r][c];
    for (int i = 0; i < c; i++) {
        printf("Enter row %i: ", i + 1);
        for (int q = 0; q < r; q++) {
            scanf("%i,",&matrix[q][i]);
        }
    }

    for(int i = 0; i < r; i++) {
        for(int q = 0; q < c; q++) {
            printf("%i\t",matrix[i][q]);
        }
        printf("\n");
    }

    return 0;
}