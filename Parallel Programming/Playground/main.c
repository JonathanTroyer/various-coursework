#include <stdio.h>
#include <omp.h>
#include <math.h>
#include <sys/time.h>
#include <stdlib.h>

int main() {

    srand((unsigned)time(NULL));
    double amt = ((double) rand() / RAND_MAX);
    int thread_count = amt * 4;

    printf("%f, %d\n\n", amt, thread_count);

    long m = 10, n = 10;

    double A[m * n], x[n], y[m];
    long i, j;

    for (i = 0; i < n; i++) {
        x[i] = ((double) rand()) / rand();
        for (j = 0; j < m; j++) {
            A[j * i + i] = ((double) rand()) / rand();
        }
    }

#   pragma omp parallel for num_threads(thread_count) \
        default(none) private(i, j) shared(A, x, y, m, n)
    for (i = 0; i < m; i++) {
        y[i] = 0.0;
        for (j = 0; j < n; j++) {
            y[i] += A[i * j + j] * x[j];
        }
    }

    for (i = 0; i < m; i++) {
        printf("%f\n", y[i]);
    }

    return 0;
}