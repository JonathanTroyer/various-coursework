# OC-Parallel-Programming-CP

This is an implementation of a parallel quicksort (specifically PSRS) for Oklahoma Christian's Parallel Programming course

### Author
Jonathan Troyer

### Current Issues
The parity of the number of threads must match the parity of the size of the array divided by the number of threads.
Need to use `memalloc` for array initialization.

### OS
Windows 10, Version 1709, Build 16299.64