#include <errno.h>
#include <omp.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <sys/time.h>
#include <memory.h>

void swap(int *a, int *b);

void print_arr(int *arr, int size);

double get_time(struct timeval t);

//From http://www.geeksforgeeks.org/quick-sort/
void qsort_s(int arr[], int low, int high) {
    if (low < high) {
        /* pi is partitioning index, arr[p] is now
           at right place */
        int pivot = arr[high];    // pivot
        int i = (low - 1);  // Index of smaller element

        for (int j = low; j <= high - 1; j++) {
            // If current element is smaller than or
            // equal to pivot
            if (arr[j] <= pivot) {
                i++;    // increment index of smaller element
                swap(&arr[i], &arr[j]);
            }
        }
        swap(&arr[i + 1], &arr[high]);
        int pi = (i + 1);

        // Separately sort elements before
        // partition and after partition
        qsort_s(arr, low, pi - 1);
        qsort_s(arr, pi + 1, high);
    }
}

//Basically a binary search to find the last split point for a value
int find_partition(int arr[], int low, int high, int val) {
    if (high - low < 2) {
        return -1;
    }
    int mid = (low + high) / 2;
    if (arr[mid] <= val) {
        if (arr[mid + 1] > val) {
            return mid;
        } else {
            return find_partition(arr, mid, high, val);
        }
    } else {
        return find_partition(arr, low, mid, val);
    }
}

//Refer to PSRS https://www.uio.no/studier/emner/matnat/ifi/INF3380/v10/undervisningsmateriale/inf3380-week12.pdf
void psrs(int *arr, int n, int p) {
    int p2 = p * p;
    int sample_width = n / p2;
    int samples[p2];
    int chunk_width = n / p;
    //Split the array into p chunks, and qsort on each chunk
#   pragma omp parallel for num_threads(p)
    for (int i = 0; i < p; i++) {
        int low = chunk_width * i;
        int high = low + chunk_width;
        qsort_s(arr, low, high-1);
        //Take regular samples from each chunk
        for (int j = 0; j < p; j++) {
            samples[i * p + j] = arr[low + sample_width * j];
        }
    }
    //Take samples, sort, and then pick pivots
    qsort_s(samples, 0, p2-1);
    int num_pivots = p - 1;
    int pivots[num_pivots];
    float pivot_width = p2 / (float)(num_pivots+1);
#   pragma omp parallel for num_threads(p)
    for (int i = 1; i <= num_pivots; i++) {
        int index = (int) (i * pivot_width);
        pivots[i-1] = samples[index];
    }

    //For each process, partition the data by the pivots and then give chunk i to process i, positioned by thread
    //Thus, thread 0's third chunk goes to thread 2 in the first position
    /*
     * First, we must find the partitions for each thread
     * I'm fairly sure there's a better way to do this:
     * http://citeseerx.ist.psu.edu/viewdoc/download;jsessionid=4414DDE4884765D9C61C8BA747138882?doi=10.1.1.31.6231&rep=rep1&type=pdf
     * Unfortunately, I could not wrap my head around it so I'm doing what I understand
     */
    int partitions[p][num_pivots + 2];
//#   pragma omp parallel for num_threads(p)
    for (int i = 0; i < p; i++) {
        int offset = i * chunk_width;
        int partition;
        //Set first and last to the chunk boundaries
        partitions[i][0] = offset;
        partitions[i][num_pivots + 1] = offset + chunk_width;
        //Binary search for partition in a chunk, starting from the last found partition
        //This is the part that I think could be better, see above paper
        int last = offset;
        for (int j = 0; j < num_pivots; j++) {
            partition = find_partition(arr, last, offset + chunk_width, pivots[j]);
            if (partition < 0) {
                partition = last;
            } else {
                partition++;
            }
            partitions[i][j + 1] = partition;
            last = partition;
        }
    }

    //Find the size of each subarray
    int sizes[p];
    memset(sizes, 0, sizeof sizes);
#   pragma omp parallel for num_threads(p)
    for (int i = 0; i < p; i++) {
        for (int j = 0; j <= num_pivots; j++) {
            sizes[j] += partitions[i][j + 1] - partitions[i][j];
        }
    }

#   pragma omp barrier
    int starts[p];
    int start_track[p];
    starts[0] = 0;
    start_track[0] = 0;
    for (int i = 0; i < p - 1; i++) {
        starts[i + 1] = sizes[i] + starts[i];
        start_track[i + 1] = sizes[i] + starts[i];
    }

    int sarr[n];
    //Finally, we put each partition into it's proper place in the correct subarray
    //This is what makes partition 2 from thread 0 go to thread 2's 0th chunk
    for (int i = 0; i < p; i++) {
        for (int j = 0; j <= num_pivots; j++) {
            int start = start_track[j];
            int partition_size = partitions[i][j + 1] - partitions[i][j];
            for (int k = 0; k <partition_size; k++) {
                sarr[start + k] = arr[k+partitions[i][j]];
            }
            start_track[j] += partition_size;
        }
    }

    //Sort each chunk
#   pragma omp parallel for num_threads(p)
    for (int i = 0; i < p; i++) {
        qsort_s(sarr, starts[i], sizes[i] + starts[i] - 1);
    }

    //Copy into old array
#   pragma omp parallel for num_threads(p)
    for(int i = 0; i < n; i ++) {
        arr[i] = sarr[i];
    }
}

int main(int argc, char *argv[]) {
    srand((unsigned int) time(NULL));
    //Defaults if not given any arguments
    int arr_size = 100000;
    int threads = 4;
    //One given number, use as array size
    if (argc == 2) {
        errno = 0;
        arr_size = (int) strtol(argv[1], NULL, 10);
        if (errno != 0) {
            perror(NULL);
            return errno;
        }
    } // Two given numbers, use as array size and thread count
    else if (argc == 3) {
        errno = 0;
        arr_size = (int) strtol(argv[1], NULL, 10);
        threads = (int) strtol(argv[2], NULL, 10);
        if (errno != 0) {
            perror(NULL);
            return errno;
        }
    } //More than two numbers (invalid number of args), so exit
    else if (argc > 3) {
        errno = 7;
        perror(NULL);
        return errno;
    }
    //Declare two arrays to be sorted, and fill them
    int arr_s[arr_size];
    int arr_p[arr_size];
    for (int i = 0; i < arr_size; i++) {
        int val = rand();
        arr_s[i] = val;
        arr_p[i] = val;
    }

    //Time sequential
    struct timeval start, end;
    gettimeofday(&start, NULL);
    qsort_s(arr_s, 0, arr_size - 1);
    gettimeofday(&end, NULL);
    double diff = get_time(end) - get_time(start);
    printf("Time: %f\n", diff);
    print_arr(arr_s, arr_size);

    //Time parallel
    gettimeofday(&start, NULL);
    psrs(arr_p, arr_size, threads);
    gettimeofday(&end, NULL);
    diff = get_time(end) - get_time(start);
    printf("Time: %f", diff);
    print_arr(arr_p, arr_size);
    return 0;
}

void swap(int *a, int *b) {
    int t = *a;
    *a = *b;
    *b = t;
}

void print_arr(int *arr, int size) {
    for (int i = 0; i < size - 1; i++) {
        printf("%d\n", arr[i]);
    }
    printf("%d\n\n", arr[size - 1]);
}

double get_time(struct timeval t) {
    return t.tv_sec + t.tv_usec / 1000000.0;
}