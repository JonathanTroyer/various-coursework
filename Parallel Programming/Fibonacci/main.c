#include <stdio.h>

int main() {
    int type;
    printf("0 - For\n1 - Recursive\nEnter the calculation type: ");
    scanf("%i", &type);
    int number;
    printf("Enter the number of iterations: ");
    scanf("%i", &number);

    if(type == 0) {
        printf("%i", fibFor(number));
    } else {
        printf("%i", fibRecursive(number));
    }

    return 0;
}

int fibFor(int num) {
    int last2 = 0, last1 = 1;
    int tmp = 0;
    for(int i = 2; i < num; i++){
        tmp = last2+last1;
        last2 = last1;
        last1 = tmp;
    }
    return last2+last1;
}

int fibRecursive(int num) {
    if(num <= 0) {
        return 0;
    } else if (num == 1) {
        return 1;
    } else {
        return fibRecursive(num-2) + fibRecursive(num-1);
    }
}