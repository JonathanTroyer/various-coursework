#include <stdio.h>
#include "omp.h"

int main() {
    omp_set_num_threads(1000);

#pragma omp parallel
    {
        printf("Hello, World! Thread : (%d)\n", omp_get_thread_num());
    }
    return 0;
}