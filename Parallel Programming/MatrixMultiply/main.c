#include <stdio.h>

int main() {
    int m1r, m1c, m2r, m2c;
    printf("Enter the first matrix size, separated by commas: ");
    scanf("%i,%i", &m1r, &m1c);
    printf("Enter the second matrix size, separated by commas: ");
    scanf("%i,%i", &m2r, &m2c);

    if(m1c != m2r) {
        printf("Cannot multiply these matrices");
        return 1;
    }
    printf("\n");

    int matrix1 [m1r][m1c];
    int matrix2 [m2r][m2c];
    for(int i = 0; i < m1r; i++) {
        printf("Enter the %i row for matrix 1: ", i+1);
        for(int q = 0; q < m1c; q++){
            scanf("%i,", &matrix1[i][q]);
        }
    }
    printf("\n");

    for(int i = 0; i < m2r; i++) {
        printf("Enter the %i row for matrix 2: ", i+1);
        for(int q = 0; q < m2c; q++){
            scanf("%i,", &matrix2[i][q]);
        }
    }
    printf("\n");

    int result [m1r][m2c];
    for(int i = 0; i < m1r; i++) {
        for(int j = 0; j < m2c; j++) {
            int sum = 0;
            for(int k = 0; k < m1c; k++) {
                sum += matrix1[i][k] * matrix2[k][j];
            }
            result[i][j] = sum;
            printf("%i\t", sum);
        }
        printf("\n");
    }

    return 0;
}