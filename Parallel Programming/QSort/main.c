#include <stdio.h>
#include <stdlib.h>
#include <errno.h>

void QSort_Sequential(int *arr[]) {}

void QSort_Parallel(int *arr[]) {}

int main(int argc, char *argv[]) {
    //Defaults if not given any arguments
    long arr_size = 1000;
    long threads = 4;
    //One given number, use as array size
    if(argc == 2){
        errno = 0;
        arr_size = strtol(argv[1], NULL, 10);
        if (errno != 0) {
            perror(NULL);
            return errno;
        }
    } // Two given numbers, use as array size and thread count
    else if(argc == 3) {
        errno = 0;
        arr_size = strtol(argv[1], NULL, 10);
        threads = strtol(argv[2], NULL, 10);
        if (errno != 0) {
            perror(NULL);
            return errno;
        }
    } //More than two numbers (invalid number of args), so exit
    else {
        errno = 7;
        perror(NULL);
        return errno;
    }
    //Declare two arrays to be sorted, and fill them
    int arr_s[arr_size];
    int arr_p[arr_size];
    for (int i = 0; i < arr_size; i++) {
        int val = rand();
        arr_s[i] = val;
        arr_p[i] = val;
    }

    return 0;
}