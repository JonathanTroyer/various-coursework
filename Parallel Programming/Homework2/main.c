#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include "omp.h"

double findTimeSpent(clock_t start, clock_t end) {
    return ((double)(end - start)) / CLOCKS_PER_SEC;
}

int main() {
    long n = rand();

    unsigned long long sum = 0;

    clock_t begin = clock();
#pragma omp parallel for num_threads(omp_get_max_threads()) \
    reduction(+: sum)
    for (unsigned long long  i = 1; i <= n; i++) {
        sum += i;
        printf("\n%lli", i);
    }
    clock_t end = clock();
    double timeSpent_p = findTimeSpent(begin, end);

    printf("\n");

    begin = clock();
    for (unsigned long long i = 1; i <= 1000000; i++) {
        sum += i;
        printf("\n%lli", i);
    }
    end = clock();
    double timeSpent_s = findTimeSpent(begin, end);

    printf("Sum of 1-%li is %lli and took %f seconds in parallel and %f seconds sequentially", n, sum, timeSpent_p, timeSpent_s);

    return 0;
}

